package ru.konungstvo.kmrp_lore.network;


import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.konungstvo.kmrp_lore.ClientProxy;


public class currentDayMessage implements IMessage {
    public int day = 0;

    public currentDayMessage() {
    }

    //public ModifierMessage(boolean modifiedDamage, int modifier, boolean bodyPart) {
    //    this(modifiedDamage, modifier, bodyPart);
    //}
    public currentDayMessage(int thirdPerson) {
        this.day = thirdPerson;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        day = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(day);
    }

    public static class currentDayMessageHandler implements IMessageHandler<currentDayMessage, IMessage>
    {
        @SideOnly(Side.CLIENT)
        @Override
        public IMessage onMessage(currentDayMessage message, MessageContext ctx)
        {
            System.out.println("сообщение " + message.day);
            ClientProxy.setCurrentDay(message.day);
            return null;
        }

    }

}
