package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.ItemLoreHandler;
import ru.konungstvo.kmrp_lore.helpers.LoreChangeLogger;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

import java.util.ArrayList;
import java.util.List;

public class RenameCommand extends CommandBase {

    @Override
    public String getName() {
        return "rename";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "хелб";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public List<String> getAliases() {
        ArrayList<String> aliases = new ArrayList<>();
        aliases.add("setname");
        return aliases;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            TextComponentString error = new TextComponentString("Слишком мало аргументов!");
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }

        String name = String.join(" ", args);
        if (!name.contains("&")) {
            name = "&f" + name;
        }

        if (!PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get())) {
            if (!name.contains("[?]"))
                name += " &7[?]";        }

        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        ItemStack before = item.copy();
        ItemLoreHandler itemLoreHandler = new ItemLoreHandler(item);
        itemLoreHandler.setName(name);

        TextComponentString answer = new TextComponentString("Имя предмета изменено.");
        answer.getStyle().setColor(TextFormatting.GRAY);
        sender.sendMessage(answer);
        LoreChangeLogger.logNameChanges(sender.getName(), before, item);
    }

}
