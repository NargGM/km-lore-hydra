package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;

import java.util.Arrays;

public class ArmorTagCommand extends CommandBase {
    public static final String NAME = "armortag";

    @Override
    public String getName() { return NAME; }

    @Override
    public String getUsage(ICommandSender sender) { return null; }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }
//        HashMap<String, String> tagmap = new HashMap<>();
//        tagmap.put("л","легкая");
//        tagmap.put("с","средняя");
//        tagmap.put("т","тяжелая");
//        tagmap.put("сл","современная легкая");
//        tagmap.put("сс","современная средняя");
//        tagmap.put("ст","современная тяжелая");

        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();

        TextComponentString result;

        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(item);
        WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(item);
        WeightHandler weightHandler = new WeightHandler(item);

        switch (args[0]) {
            case "add":
                if (args.length < 3) {
                    // TODO
                }
                String weapontagString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                weapontagString = weapontagString.replace("\"t\":\"л\"","\"t\":\"легкая\"").replace("\"t\":\"с\"","\"t\":\"средняя\"").replace("\"t\":\"т\"","\"t\":\"тяжелая\"")
                        .replace("\"t\":\"сл\"","\"t\":\"современная легкая\"").replace("\"t\":\"сс\"","\"t\":\"современная средняя\"").replace("\"t\":\"ст\"","\"t\":\"современная тяжелая\"");
                NBTTagCompound weapontag;
                try {
                    weapontag = JsonToNBT.getTagFromJson(weapontagString);
                } catch (NBTException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                    return;
                }


                NBTTagCompound armor = weapontag.getCompoundTag("Броня").getCompoundTag("armor");
                int number = armor.getKeySet().size();
                double weight = 0;
                double dur = 0;

                for (String key : armor.getKeySet()) {
                    NBTTagCompound segment = armor.getCompoundTag(key);
                    weight += segment.getDouble("w");
                    dur += segment.getInteger("d");
                    armor.getCompoundTag(key).setInteger("cd", segment.getInteger("d"));
                }

                System.out.println(weight);
                weaponTagsHandler.addWeaponTag(weapontag);
                dur = Math.floor(dur/number);
                weight = Math.ceil(weight);


                //weaponDurabilityHandler.setMaxDurability((int)dur);
                weightHandler.setWeight((int) weight);

                result = new TextComponentString("Лор брони записан.");

                //DEBUG
                //result.appendSibling(new TextComponentString("\n"+weapontagString));
                //result.appendSibling(new TextComponentString("\n"+weapontag.toString()));

                result.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(result);
                return;
            case "del":

                if (args.length < 2) {
                    // TODO
                }

                try {
                    weaponTagsHandler.delWeaponTag("Броня");
                    result = new TextComponentString("Лор брони удалён.");
                    result.getStyle().setColor(TextFormatting.GRAY);
                    sender.sendMessage(result);
                } catch (LoreException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                }
                return;

        }
    }
}
