package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.AttachmentsHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

public class BoltCommand extends CommandBase {
    @Override
    public String getName() {
        return "bolt";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        int hand = 1;
        if (args.length > 0) {
            hand = Integer.parseInt(args[0]);
        }
        executeForEntity(sender, getCommandSenderAsPlayer(sender), hand);
    }

    public static void executeForEntity(ICommandSender sender, EntityLivingBase entity, int hand) {
        System.out.println("executing bolt for " + entity.getName() + " from " + sender.getName());

        ItemStack gun = entity.getHeldItemMainhand();
        if (hand == 0) gun = entity.getHeldItemOffhand();


        if (!WeaponTagsHandler.hasWeaponTags(gun)) {
            TextComponentString error = new TextComponentString("Нет огнестрельного оружия в руках.");
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }

        WeaponTagsHandler weapon = new WeaponTagsHandler(gun);
        if (weapon.isFull()) {
//            TextComponentString error = new TextComponentString("Оружие не может вместить больше патронов.");
//            error.getStyle().setColor(TextFormatting.RED);
//            sender.sendMessage(error);
            return;
        }

        NBTTagCompound magazineTag = weapon.getMagazine();
//            System.out.println("magazine: " + magazineTag);
        if (magazineTag == null || magazineTag.hasNoTags() || magazineTag.toString().equals("{}")) return;

        NBTTagList loadedStuff = magazineTag.getTagList(AttachmentsHandler.LOADED_PROJECTILES, 10);
        if (loadedStuff.tagCount() == 0) {
            TextComponentString error = new TextComponentString("Вставленный магазин пуст.");
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }
        //AttachmentsHandler magazine = new AttachmentsHandler(weapon.getMagazine());
        System.out.println("loadedStuff: " + loadedStuff);
        weapon.loadProjectileToChamber(loadedStuff.getCompoundTagAt(loadedStuff.tagCount() - 1));
        loadedStuff.removeTag(loadedStuff.tagCount() - 1);
        magazineTag.setTag(AttachmentsHandler.LOADED_PROJECTILES, loadedStuff);
    }
}
