package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.EquipmentBuffTagHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.SubcompEq;


public class EquipmentBuffExecutor extends CommandBase {


    @Override
    public String getName() {
        return "equipmentbuff";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return (PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get()));
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        switch (args[0]) {
            case "set":
                if (args.length < 4) return;
                switch (args[1]) {
                    case "common":
                        ItemStack item11 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                        EquipmentBuffTagHandler eb = new EquipmentBuffTagHandler(item11);
                        if (args[3].contains("%")) {
                            eb.setBuff(SubcompEq.SUBCOMPOUND_COMMON, args[2].replace("_"," "), args[3]);
                        } else {
                            eb.setBuff(SubcompEq.SUBCOMPOUND_COMMON, args[2].replace("_"," "), Integer.parseInt(args[3]));
                        }
                        break;
                    case "equipment":
                        ItemStack item12 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                        EquipmentBuffTagHandler eb1 = new EquipmentBuffTagHandler(item12);
                        if (args[3].contains("%")) {
                            eb1.setBuff(SubcompEq.SUBCOMPOUND_EQUIPMENT, args[2].replace("_"," "), args[3]);
                        } else {
                            eb1.setBuff(SubcompEq.SUBCOMPOUND_EQUIPMENT, args[2].replace("_"," "), Integer.parseInt(args[3]));
                        }
                        break;
                    case "inventory":
                        ItemStack item13 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                        EquipmentBuffTagHandler eb2 = new EquipmentBuffTagHandler(item13);
                        if (args[3].contains("%")) {
                            eb2.setBuff(SubcompEq.SUBCOMPOUND_INVENTORY, args[2].replace("_"," "), args[3]);
                        } else {
                            eb2.setBuff(SubcompEq.SUBCOMPOUND_INVENTORY, args[2].replace("_"," "), Integer.parseInt(args[3]));
                        }
                        break;
                }
                return;
            case "remove":
                ItemStack item1 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                EquipmentBuffTagHandler wh1 = new EquipmentBuffTagHandler(item1);
                wh1.removeEbTag();
                return;
        }
    }
}
