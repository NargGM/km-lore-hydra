package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.CostHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

import java.util.Arrays;

public class SetCostCommand extends CommandBase {
    @Override
    public String getName() {
        return "setcost";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return (PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get()));
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }



    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) return;
        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        CostHandler costHandler = new CostHandler(item);
        if (args[0].equals("remove")) {
            costHandler.removeCost();
            return;
        }
        int cost = Integer.parseInt(args[0]);
        String category = "";
        if (args.length > 1) category = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        costHandler.setCost(cost, category);
    }
}
