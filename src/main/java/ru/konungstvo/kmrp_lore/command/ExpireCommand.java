package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.CostHandler;
import ru.konungstvo.kmrp_lore.helpers.ExpireDate;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

import java.util.Arrays;

public class ExpireCommand extends CommandBase {
    @Override
    public String getName() {
        return "setexpire";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return (PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get()));
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }



    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) return;
        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        ExpireDate expireDate = new ExpireDate(item);
        if (args[0].equals("reset")) {
            expireDate.resetExpiration();
            return;
        }
        if (args[0].equals("neverexpires")) {
            if (expireDate.getTrueExpire() == -666) expireDate.setExpire(8);
            expireDate.setNeverExpires(true);
            return;
        }
        if (args[0].equals("expires")) {
            if (expireDate.getTrueExpire() == -666) expireDate.setExpire(8);
            expireDate.setNeverExpires(false);
            return;
        }
        if (args[0].equals("nutrition")) {
            if (args.length < 2) return;
            if (expireDate.getTrueExpire() == -666) expireDate.setExpire(8);
            expireDate.setNutritionValue(Integer.parseInt(args[1]));
            return;
        }
        expireDate.setExpire(Integer.parseInt(args[0]));
    }
}
