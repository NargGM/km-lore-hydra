package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

import java.util.ArrayList;
import java.util.Arrays;


public class ArmorDurabilityHandler extends WeaponTagsHandler {

    public ArmorDurabilityHandler(ItemStack itemStack) {
        super(itemStack);
    }

    public boolean hasArmorDict() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        if (getDefaultWeapon() == null) return false;
        return getDefaultWeapon().hasKey("armor");
    }


    public int getPercentageRatioForSegment(String segment) {
        NBTTagCompound armor = getDefaultWeapon().getCompoundTag("armor");
        if (!armor.hasKey(segment)) return 100;
        NBTTagCompound segmenttc = armor.getCompoundTag(segment);

        int durability = segmenttc.getInteger("cd");
        int maxDurability = segmenttc.getInteger("d");
        float result = (float) durability / maxDurability;
        return Math.round(result * 100);
    }

    public int getDurabilityForSegment(String segment) {
        NBTTagCompound armor = getDefaultWeapon().getCompoundTag("armor");
        if (!armor.hasKey(segment)) return 100;
        NBTTagCompound segmenttc = armor.getCompoundTag(segment);

        return segmenttc.getInteger("cd");
    }
    public String getToolTipByPercentage(int percentageRatio) {
        return null;
    }

    public void takeAwayDurabilityForSegment(String segment, int loss) {
        System.out.println("ARMORTEST " + segment);
        NBTTagCompound armor = getDefaultWeapon().getCompoundTag("armor");
        if (!armor.hasKey(segment)) return;
        NBTTagCompound segmenttc = armor.getCompoundTag(segment);

        int durability = segmenttc.getInteger("cd");
        System.out.println("ARMORTEST " + durability);
        int newDurability = durability - loss;
        if (newDurability < 0) {
            if (Math.random() != 0.0) newDurability = 0;
        }
        System.out.println("ARMORTEST " + newDurability);
        //durabilityDict.setInteger("durability", newDurability);
        //getWeaponDurabilityDict(weapon).merge(durabilityDict);
//        tagCompound.getCompoundTag(COMPOUND_NAME).getCompoundTag(weapon).getCompoundTag("durabilityDict").setInteger("durability", newDurability);
        segmenttc.setInteger("cd", newDurability);
    }

    public void takeAwayDurabilityForAllSegments(int loss) {
        NBTTagCompound armor = getDefaultWeapon().getCompoundTag("armor");
        for (String segment : armor.getKeySet()) {
            NBTTagCompound segmenttc = armor.getCompoundTag(segment);

            int durability = segmenttc.getInteger("cd");
            System.out.println("ARMORTEST " + durability);
            int newDurability = durability - loss;
            if (newDurability < 0) {
                if (Math.random() != 0.0) newDurability = 0;
            }
            System.out.println("ARMORTEST " + newDurability);
            //durabilityDict.setInteger("durability", newDurability);
            //getWeaponDurabilityDict(weapon).merge(durabilityDict);
//        tagCompound.getCompoundTag(COMPOUND_NAME).getCompoundTag(weapon).getCompoundTag("durabilityDict").setInteger("durability", newDurability);
            segmenttc.setInteger("cd", newDurability);
        }
    }
    public String countString(String segment) {
        int percentageRatio = getPercentageRatioForSegment(segment);
        String color2 = "";
        if (percentageRatio > 75) {
            color2 = "§2";
        } else if (percentageRatio > 50) {
            color2 = "§e";
        } else if (percentageRatio > 25) {
            color2 = "§7";
        } else if (percentageRatio > 0) {
            color2 = "§c";
        } else if (percentageRatio == 0) {
            color2 = "§4";
        } else if (percentageRatio < 0) {
            color2 = "§8";
            if (getDurabilityForSegment(segment) < -600) {
                color2 = "§0";
            } else {
                color2 = "§8";
            }
        }
        return color2;
    }

    public ArrayList<String> splitArmorLore(String segment, int lineLength) {
        StringBuilder line = new StringBuilder();
        ArrayList<String> result = new ArrayList<>();
        char lastColor = 'f';
        NBTTagCompound armor = getDefaultWeapon().getCompoundTag("armor");
        if (!armor.hasKey(segment)) return result;
        NBTTagCompound segmenttc = armor.getCompoundTag(segment);
        if (!segmenttc.hasKey("l")) return result;
        String lore = segmenttc.getString("l");
        if (lore.length() < lineLength) {
            result.add(lore);
            return result;
        }

        for (String word : lore.split(" ")) {
            if (line.length() <= lineLength) {
                // Build a line till it reaches maximum length
                line.append(word).append(" ");
                if (word.contains("§")) {
                    lastColor = word.charAt(word.lastIndexOf("§")+1);
                }
            } else {
                // When line reaches maximum length, append it to lore and start new line with corresponding color
                result.add(line.toString());
                line = new StringBuilder("§" + lastColor + word + " ");
            }
        }
        result.add(line.toString().trim());
        return result;
    }

    public ArrayList<String> splitArmorLore2(String segment) {
        ArrayList<String> result = new ArrayList<>(Arrays.asList(segment.split(";;;")));
        return result;
    }

    }
