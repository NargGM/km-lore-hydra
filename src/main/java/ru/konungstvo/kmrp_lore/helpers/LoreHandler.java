package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

public class LoreHandler {
    ItemStack itemStack;
    NBTTagCompound tagCompound;

    public LoreHandler(ItemStack itemStack) {
        this.itemStack = itemStack;

        if (this.itemStack.getTagCompound() != null) {
            this.tagCompound = this.itemStack.getTagCompound();
        } else {
            this.tagCompound = new NBTTagCompound();
        }
        itemStack.setTagCompound(this.tagCompound);
    }


    public static NBTTagList splitLore(String lore, int lineLength) {
        NBTTagList result = new NBTTagList();
        return splitLore(lore, lineLength, result);

    }

    public static NBTTagList splitLore(String lore, int lineLength, NBTTagList result) {
        StringBuilder line = new StringBuilder();
        char lastColor = 'f';

        if (lore.length() < lineLength) {
            result.appendTag(new NBTTagString(lore));
            return result;
        }

        for (String word : lore.split(" ")) {
            if (line.length() <= lineLength) {
                // Build a line till it reaches maximum length
                line.append(word).append(" ");
                if (word.contains("§")) {
                    lastColor = word.charAt(word.lastIndexOf("§") + 1);
                }
            } else {
                // When line reaches maximum length, append it to lore and start new line with corresponding color
                result.appendTag(new NBTTagString(line.toString().trim()));
                line = new StringBuilder("§" + lastColor + word + " ");
            }
        }
        result.appendTag(new NBTTagString(line.toString().trim()));
        return result;
    }

    public static String splitLoreAsString(String lore, int lineLength) {
        StringBuilder line = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char lastColor = 'f';

        if (lore.length() < lineLength) {

            result.append(lore);
            return result.toString();
        }

        for (String word : lore.split(" ")) {
            if (line.length() <= lineLength) {
                // Build a line till it reaches maximum length
                line.append(word).append(" ");
                if (word.contains("§")) {
                    lastColor = word.charAt(word.lastIndexOf("§") + 1);
                }
            } else {
                // When line reaches maximum length, append it to lore and start new line with corresponding color
                result.append(line.toString().trim() + "\n");
                line = new StringBuilder("§" + lastColor + word + " ");
            }
        }
        result.append(line.toString().trim());
        return result.toString();
    }

}
