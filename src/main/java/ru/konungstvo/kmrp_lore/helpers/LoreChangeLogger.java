package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LoreChangeLogger {
    private static final String path = "logs/lore/lore.txt";

    /**
     * Use Streams when you are dealing with raw data
     */
    private static void write(String data) {
        try {
            BufferedWriter out = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(path, true), // true to append
                            StandardCharsets.UTF_8                  // Set encoding
                    )
            );
            out.write(data);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void logNameChanges(String playerName, ItemStack before, ItemStack after) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        String beforeName =before.getDisplayName();
        String afterName = after.getDisplayName();
        String result = dtf.format(now) + " [" + playerName + "] переименовывает " + beforeName + " в " + afterName + "\n";
        System.out.println(result);
        write(result);
    }

    public static void logLoreChanges(String playerName, ItemStack before, ItemStack after) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String result = dtf.format(now) + " [" + playerName + "] меняет " + before.getDisplayName() + " :" +
                "\nБыло: " + before.getTagCompound() + "\nСтало: " + after.getTagCompound() + "\n";

        write(result);
    }
}
