package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WeightHandler extends LoreHandler {
    public static final String WEIGHT = "weight";
    public static final String PSYCHWEIGHT = "weightpsych";
    private static final Pattern pattern = Pattern.compile("weight:[0-9]+");

    public WeightHandler(ItemStack itemStack) {
        super(itemStack);
    }
    public int getWeight() {
        if(itemStack.getTagCompound() == null) return 0;
        if(!itemStack.getTagCompound().hasKey(WEIGHT)) return 0;
        return itemStack.getTagCompound().getInteger(WEIGHT);
    }

    public int getPsychWeight() {
        if(itemStack.getTagCompound() == null) return 0;
        if(!itemStack.getTagCompound().hasKey(PSYCHWEIGHT)) return 0;
        return itemStack.getTagCompound().getInteger(PSYCHWEIGHT);
    }

    public int getWeightGun() {
        if(itemStack.getTagCompound() == null) return 0;
        int weight = 0;
        if(itemStack.getTagCompound().hasKey(WEIGHT)) weight += itemStack.getTagCompound().getInteger(WEIGHT);
        if(itemStack.getTagCompound().hasKey("attachments")) {
            for(String attach : itemStack.getTagCompound().getCompoundTag("attachments").getKeySet()) {
                NBTTagCompound attachment = itemStack.getTagCompound().getCompoundTag("attachments").getCompoundTag(attach);
                if (attachment.hasKey(WEIGHT)) weight += attachment.getInteger(WEIGHT);
            }
        }
        return weight;
    }

    public int getWeightAnything() {
        if(itemStack.getTagCompound() == null) return 0;
        int weight = 0;
        if ((itemStack.getTagCompound().hasKey("BigBag") || itemStack.getTagCompound().hasKey("Bag") || itemStack.getTagCompound().hasKey("BlockEntityTag"))) {
            Matcher matcher = pattern.matcher(itemStack.getTagCompound().toString());
            while (matcher.find()) {
                weight += Integer.parseInt(matcher.group().split(":")[1]);
            }
        } else {
            if(itemStack.getTagCompound().hasKey(WEIGHT)) weight += itemStack.getTagCompound().getInteger(WEIGHT);
            if(itemStack.getTagCompound().hasKey("attachments")) {
                for(String attach : itemStack.getTagCompound().getCompoundTag("attachments").getKeySet()) {
                    NBTTagCompound attachment = itemStack.getTagCompound().getCompoundTag("attachments").getCompoundTag(attach);
                    if (attachment.hasKey(WEIGHT)) weight += attachment.getInteger(WEIGHT);
                }
            }
        }
        return weight;
    }

    public boolean hasWeight() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        return this.itemStack.getTagCompound().hasKey(WEIGHT);
    }

    public static boolean hasWeight(ItemStack stack) {
        if (stack.getTagCompound() == null) return false;
        return stack.getTagCompound().hasKey(WEIGHT);
    }

    public void setWeight(int weight) {
        tagCompound.setInteger(WEIGHT, weight);
    }

    public void setPsychweight(int weight) {
        tagCompound.setInteger(PSYCHWEIGHT, weight);
    }

    public void removeWeight() {
        tagCompound.removeTag(WEIGHT);
    }
}
