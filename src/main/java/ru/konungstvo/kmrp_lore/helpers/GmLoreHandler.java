package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import ru.konungstvo.kmrp_lore.command.LoreException;

import java.util.Arrays;

public class GmLoreHandler extends LoreHandler {
    final int lineLength = 40;

    public GmLoreHandler(ItemStack itemStack) {
        super(itemStack);
    }

    public NBTTagList getGmLoreOrCreate() {
        if (tagCompound.hasKey("gmlore")) {
            return tagCompound.getTagList("gmlore", 8);
        }
        NBTTagList gmlore = new NBTTagList();
        this.tagCompound.setTag("gmlore", gmlore);
        return gmlore;
    }

    public void setGmLore(String[] args) throws NumberFormatException, LoreException {
        int pos;
        pos = Integer.parseInt(args[1]);

        String line = String.join(" ", Arrays.copyOfRange(args,2, args.length));
        if (!line.contains("&")) {
            line = "&f" + line;
        }
        line = line.replaceAll("&", "§");

        if (line.length() > lineLength) {
            String newline = "";
            for (String word : line.split(" ")) {
                if (newline.length() <= lineLength) {
                    newline += word + " ";
                } else {
                    line = newline.trim();
                    break;
                }
            }
        }

        if (pos > getGmLoreOrCreate().tagCount()) {
            throw new LoreException("Указанное число больше, чем количество строк в лоре.");
        }
        getGmLoreOrCreate().set(pos-1, new NBTTagString(line));
    }
    public NBTTagList getGmLore() {
        if (tagCompound.hasKey("gmlore")) {
            return tagCompound.getTagList("gmlore", 8);
        }
        return null;
    }

    public void addGmLore(String addgmlore) {
        addgmlore = addgmlore.replaceAll("&", "§");

        NBTTagList gmlore = getGmLoreOrCreate();
        splitGmLore(addgmlore, lineLength, gmlore);

        tagCompound.setTag("gmlore", gmlore);

        this.itemStack.setTagCompound(tagCompound);
        System.out.println(itemStack.getTagCompound().toString());
    }

    public void delGmLore(int pos) throws LoreException {
        if (this.tagCompound == null) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        if (tagCompound.hasNoTags() || !tagCompound.hasKey("gmlore") || getGmLoreOrCreate().tagCount() == 0) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        if (pos > getGmLoreOrCreate().tagCount()) {
            pos = getGmLoreOrCreate().tagCount();
        }
        getGmLoreOrCreate().removeTag(pos-1);
    }


    public void purgeGmLore() throws LoreException {
        purgeGmLore(false);
    }

    public void purgeGmLore(boolean newlore) throws LoreException {
        if (!newlore && this.tagCompound == null) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        if (!newlore && (tagCompound.hasNoTags() || !tagCompound.hasKey("gmlore"))) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        tagCompound.removeTag("gmlore");
    }

    public boolean hasGmLore() {
        return false;
    }

    public static boolean hasGmLore(ItemStack itemStack) {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        return itemStack.getTagCompound().hasKey("gmlore");
    }

    public static NBTTagList splitGmLore(String lore, int lineLength, NBTTagList result) {
        StringBuilder line = new StringBuilder();
        char lastColor = 'f';

        if (lore.length() < lineLength) {
            result.appendTag(new NBTTagString(lore));
            return result;
        }

        for (String word : lore.split(" ")) {
            if (line.length() <= lineLength) {
                // Build a line till it reaches maximum length
                line.append(word).append(" ");
                if (word.contains("§")) {
                    lastColor = word.charAt(word.lastIndexOf("§")+1);
                }
            } else {
                // When line reaches maximum length, append it to lore and start new line with corresponding color
                result.appendTag(new NBTTagString(line.toString().trim()));
                line = new StringBuilder("§" + lastColor + word + " ");
            }
        }
        result.appendTag(new NBTTagString(line.toString().trim()));
        return result;
    }
}
