package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import ru.konungstvo.kmrp_lore.CommonProxy;

public class ExpireDate extends LoreHandler {
    public static final String EXPIRE = "expiretag";

    public ExpireDate(ItemStack itemStack) {
        super(itemStack);
    }
    public NBTTagCompound getExpireTagOrCreate() {
        if (tagCompound.hasKey(EXPIRE)) {
            return tagCompound.getCompoundTag(EXPIRE);
        }
        NBTTagCompound costTags = new NBTTagCompound();
        this.tagCompound.setTag(EXPIRE, costTags);
        return costTags;
    }

    public static boolean hasExpiration(ItemStack stack) {
        if (stack.getTagCompound() == null) return false;
        return stack.getTagCompound().hasKey(EXPIRE);
    }

    public boolean isExpired() {
        return !neverExpired() && getExpireDate() != -666 && getExpireDate() <= CommonProxy.getCurrentDay();

    }

    public void setExpire(int days) {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        costCompound.setInteger("expireDays", days);
    }

    public int getExpire() {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        if (!costCompound.hasKey("expireDays") || neverExpired()) return -666;
        return costCompound.getInteger("expireDays");
    }

    public int getTrueExpire() {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        if (!costCompound.hasKey("expireDays")) return -666;
        return costCompound.getInteger("expireDays");
    }

    public void setExactExpireDate(int date) {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        costCompound.setInteger("expireAt", date);
    }

    public int getExpireDate() {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        if (!costCompound.hasKey("expireAt") || neverExpired()) return -666;
        return costCompound.getInteger("expireAt");
    }

    public void setNeverExpires(boolean neverExpires) {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        costCompound.setBoolean("neverExpires", neverExpires);
    }

    public boolean neverExpired() {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        if (!costCompound.hasKey("neverExpires")) return false;
        return costCompound.getBoolean("neverExpires");
    }

    public void setNutritionValue(int nutritionValue) {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        costCompound.setInteger("nutritionValue", nutritionValue);
    }

    public int getNutritionValue() {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        if (!costCompound.hasKey("nutritionValue")) return -666;
        return costCompound.getInteger("nutritionValue");
    }

    public void startExpiration(int day) {
        setExactExpireDate(day + getExpire());
    }

    public void resetExpiration() {
        NBTTagCompound costCompound = getExpireTagOrCreate();
        costCompound.removeTag("expireAt");
    }

    public void removeCost() {
        tagCompound.removeTag(EXPIRE);
    }
}
