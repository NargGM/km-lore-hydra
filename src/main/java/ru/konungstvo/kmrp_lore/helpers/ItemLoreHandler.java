package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import ru.konungstvo.kmrp_lore.command.LoreException;

import java.util.Arrays;
import java.util.Iterator;

public class ItemLoreHandler extends LoreHandler {
    final int lineLength = 40;

    public ItemLoreHandler(ItemStack itemStack) {
        super(itemStack);
    }

    public NBTTagCompound getDisplayOrCreate() {
        if (tagCompound.hasKey("display")) {
            return tagCompound.getCompoundTag("display");
        }
        NBTTagCompound display = new NBTTagCompound();
        this.tagCompound.setTag("display", display);
        return display;
    }

    public NBTTagList getLoreOrCreate() {
        if (getDisplayOrCreate().hasKey("Lore")) {
            return getDisplayOrCreate().getTagList("Lore", 8);
        }
        NBTTagList lore = new NBTTagList();
        getDisplayOrCreate().setTag("Lore", lore);
        return lore;
    }

    public void setName(String name) {
        name = name.replaceAll("&", "§");
        this.itemStack.setStackDisplayName(name);
    }

    public String getName() {
        return this.itemStack.getDisplayName();
    }

    public void addLore(String addlore) {
        addlore = addlore.replaceAll("&", "§");

        NBTTagList lore = getLoreOrCreate();
        splitLore(addlore, lineLength, lore);

        NBTTagCompound display = getDisplayOrCreate();
        display.setTag("Lore", lore);
        tagCompound.setTag("display", display);

        //this.itemStack.setTagCompound(tagCompound);
//        tagCompound.merge(display);
        this.itemStack.setTagCompound(tagCompound);
        System.out.println(itemStack.getTagCompound().toString());
    }

    public void purgeLore() throws LoreException {
        purgeLore(false);
    }

    public void purgeLore(boolean newlore) throws LoreException {
        if (!newlore && this.tagCompound == null) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        if (!newlore && (getDisplayOrCreate().hasNoTags() || !getDisplayOrCreate().hasKey("Lore"))) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        getDisplayOrCreate().removeTag("Lore");
    }



    public void setLore(String[] args) throws NumberFormatException, LoreException {
        int pos;
        pos = Integer.parseInt(args[0]);

        String line = String.join(" ", Arrays.copyOfRange(args,1, args.length));
        if (!line.contains("&")) {
            line = "&f" + line;
        }
        line = line.replaceAll("&", "§");

        if (line.length() > lineLength) {
            String newline = "";
            for (String word : line.split(" ")) {
                if (newline.length() <= lineLength) {
                    newline += word + " ";
                } else {
                    line = newline.trim();
                    break;
                }
            }
        }

        if (pos > getLoreOrCreate().tagCount()) {
            throw new LoreException("Указанное число больше, чем количество строк в лоре.");
        }
        getLoreOrCreate().set(pos-1, new NBTTagString(line));
    }

    public void delLore(int pos) throws LoreException {
        if (this.tagCompound == null) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        if (getDisplayOrCreate().hasNoTags() || !getDisplayOrCreate().hasKey("Lore") || getLoreOrCreate().tagCount() == 0) {
            throw new LoreException("У предмета и без того нет лора.");
        }

        if (pos > getLoreOrCreate().tagCount()) {
            pos = getLoreOrCreate().tagCount();
        }
        getLoreOrCreate().removeTag(pos-1);
    }

    public void insertLore(int pos, String inlore) throws LoreException {
        if (pos > getLoreOrCreate().tagCount()) {
            throw new LoreException("Указанное число больше количества строк лора.");
        }
        if (!inlore.contains("&")) {
            inlore = "&f" + inlore;
        }
        inlore = inlore.replaceAll("&", "§");

        NBTTagList lore = getLoreOrCreate();
        NBTTagList newlore = new NBTTagList();

        int count = 0;
        for (Iterator<NBTBase> it = lore.iterator(); it.hasNext(); count++) {
            if (count == pos-1) {
                splitLore(inlore, lineLength, newlore);
            }
            newlore.appendTag(it.next());
        }
        getDisplayOrCreate().setTag("Lore", newlore);
    }
}
