package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CostHandler extends LoreHandler {
    public static final String COST = "costtag";

    public CostHandler(ItemStack itemStack) {
        super(itemStack);
    }
    public String getCostString() {
        if(itemStack.getTagCompound() == null) return "";
        if(!itemStack.getTagCompound().hasKey(COST)) return "";
        NBTTagCompound cost = itemStack.getTagCompound().getCompoundTag(COST);
        return cost.getInteger("cost") + " " + cost.getTagList("category", 8);
    }

    public String getCostStringMini() {
        if(itemStack.getTagCompound() == null) return "";
        if(!itemStack.getTagCompound().hasKey(COST)) return "";
        NBTTagCompound cost = itemStack.getTagCompound().getCompoundTag(COST);
        return " " + cost.getTagList("category", 8);
    }

    public NBTTagCompound getCostTagOrCreate() {
        if (tagCompound.hasKey(COST)) {
            return tagCompound.getCompoundTag(COST);
        }
        NBTTagCompound costTags = new NBTTagCompound();
        this.tagCompound.setTag(COST, costTags);
        return costTags;
    }

    public boolean hasWeight() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        return this.itemStack.getTagCompound().hasKey(COST);
    }

    public static boolean hasCost(ItemStack stack) {
        if (stack.getTagCompound() == null) return false;
        return stack.getTagCompound().hasKey(COST);
    }

    public void setCost(int cost, String category) {
        NBTTagCompound costCompound = getCostTagOrCreate();
        costCompound.setInteger("cost", cost);
        NBTTagList categorylist = new NBTTagList();
        for(String cat : category.split(";")) {
            categorylist.appendTag(new NBTTagString(cat.trim()));
        }
        costCompound.setTag("category", categorylist);
    }


    public void removeCost() {
        tagCompound.removeTag(COST);
    }
}
