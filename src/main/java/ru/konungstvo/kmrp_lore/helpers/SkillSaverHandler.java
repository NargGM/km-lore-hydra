package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class SkillSaverHandler extends LoreHandler {
    public static final String SKILL_SAVER = "skillSaver";

    public SkillSaverHandler(ItemStack itemStack) {
        super(itemStack);
    }

    public NBTTagCompound getCompoundOrCreate() {
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getDefaultWeapon();
        if (weapon.hasKey(SKILL_SAVER)) {
            return weapon.getCompoundTag(SKILL_SAVER);
        }
        NBTTagCompound ssv = new NBTTagCompound();
        weapon.setTag(SKILL_SAVER, ssv);
        return ssv;
    }

    public NBTTagCompound getCompoundOrCreateForWeapon(String weaponName) {
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getWeaponByName(weaponName);
        if (weapon.hasKey(SKILL_SAVER)) {
            return weapon.getCompoundTag(SKILL_SAVER);
        }
        NBTTagCompound ssv = new NBTTagCompound();
        weapon.setTag(SKILL_SAVER, ssv);
        return ssv;
    }

    public void purge() {
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapons = weaponTagsHandler.getWeaponTags();
        for (String weaponKey : weapons.getKeySet()) {
            weapons.getCompoundTag(weaponKey).removeTag(SKILL_SAVER);
        }
    }

    public void setSkillSaver(String name, String type, String skill) {
        NBTTagCompound ssv = getCompoundOrCreate();
        System.out.println("ssv main compound: " + ssv.toString());
        NBTTagCompound playerCompound = ssv.getCompoundTag(name);
        System.out.println("ssv player compound: " + playerCompound.toString());
        playerCompound.setString(type, skill);
        ssv.setTag(name, playerCompound);
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapontag = weaponTagsHandler.getDefaultWeapon();
        weapontag.setTag(SKILL_SAVER, ssv);
//        NBTTagCompound weapontags = weaponTagsHandler.getWeaponTagsOrCreate();
//        weapontags.setTag(weaponTagsHandler.getDefaultTag(), weapontag);
        tagCompound.getCompoundTag(WeaponTagsHandler.COMPOUND_NAME).setTag(weaponTagsHandler.getDefaultWeaponName(), weapontag);
//        System.out.println("tagCompound[\"SKILLSAVER\"]: " + this.tagCompound.getCompoundTag(SKILL_SAVER).toString());
//        System.out.println("itemStack: " + this.itemStack.toString());


//      getCompoundOrCreate().setString(name, skill);
    }

    @Deprecated
    public NBTTagCompound getFullSkillSaverFor(String name) {
        return getCompoundOrCreate().getCompoundTag(name);
        //return getCompoundOrCreate().getString(name);
    }

    public NBTTagCompound getFullSkillSaverForWeapon(String name, String weaponName) {
        return getCompoundOrCreateForWeapon(weaponName).getCompoundTag(name);
        //return getCompoundOrCreate().getString(name);
    }

    public String getSkillSaverFor(String name, String type) {
        return getCompoundOrCreate().getCompoundTag(name).getString(type);
        //return getCompoundOrCreate().getString(name);
    }

    public boolean isEmpty() {
        if (itemStack.getTagCompound() == null) {
            return true;
        }
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getDefaultWeapon();
        return !weapon.hasKey(SKILL_SAVER);
    }

    public static boolean hasCompound(ItemStack itemStack) {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        if (!weaponTagsHandler.hasWeaponTags()) return false;

        return weaponTagsHandler.getDefaultWeapon().hasKey(SKILL_SAVER);
    }


}
