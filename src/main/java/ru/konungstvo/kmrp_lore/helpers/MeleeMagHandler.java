package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import static ru.konungstvo.kmrp_lore.helpers.AttachmentsHandler.LOADED_PROJECTILES;

public class MeleeMagHandler extends LoreHandler {
    public static final String MELEE_MAG = "meleemag";

    public MeleeMagHandler(ItemStack itemStack) {
        super(itemStack);
    }
    public NBTTagCompound getMeleeMagOrCreate() {
        if (tagCompound.hasKey(MELEE_MAG)) {
            return tagCompound.getCompoundTag(MELEE_MAG);
        }
        NBTTagCompound costTags = new NBTTagCompound();
        this.tagCompound.setTag(MELEE_MAG, costTags);
        return costTags;
    }

    public static boolean hasMeleeMag(ItemStack stack) {
        if (stack.getTagCompound() == null) return false;
        return stack.getTagCompound().hasKey(MELEE_MAG);
    }

    public void setMeleeMag(int capacity, String caliber) {
        NBTTagCompound costCompound = getMeleeMagOrCreate();
        costCompound.setInteger("capacity", capacity);
        costCompound.setString("caliber", caliber);
    }

    public int getCapacity() {
        NBTTagCompound costCompound = getMeleeMagOrCreate();
        return costCompound.getInteger("capacity");
    }

    public String getCaliber() {
        NBTTagCompound costCompound = getMeleeMagOrCreate();
        return costCompound.getString("caliber");
    }

    public void remove() {
        tagCompound.removeTag(MELEE_MAG);
    }

    public boolean isLoaded() {
        return getMeleeMagOrCreate().getTagList(LOADED_PROJECTILES, 10).tagCount() > 0;
    }

    public boolean fireMelee() {
        System.out.println("megatest " + getMeleeMagOrCreate().getTagList(LOADED_PROJECTILES, 10).tagCount());
        if (!isLoaded()) return false;
        NBTTagCompound weapontags = getMeleeMagOrCreate();
        NBTTagList chamber = weapontags.getTagList(LOADED_PROJECTILES, 10);

        System.out.println("Firing " + chamber.get(chamber.tagCount() - 1));
        chamber.removeTag(chamber.tagCount() - 1);
        weapontags.setTag(LOADED_PROJECTILES, chamber);

        this.tagCompound.setTag(MELEE_MAG, weapontags);
        return true;
    }

    public boolean doesProjectileFit(NBTTagCompound projectile) {
        return getCaliber().equals(projectile.getString("caliber"));
    }

    public boolean isFull() {
        int capacity = getCapacity();
        //System.out.println("capacity: " + capacity);
        return getMeleeMagOrCreate().getTagList(LOADED_PROJECTILES, 10).tagCount() >= capacity;
    }

    public void loadProjectile(NBTTagCompound projectile, String itemID) {
        projectile = projectile.copy();
        projectile.setString("itemrl", itemID);
        if (itemStack.getTagCompound() != null) {
            NBTTagCompound main = getMeleeMagOrCreate();
            //NBTTagCompound attach = main.getCompoundTag(ATTACHABLE);
            NBTTagList loaded = main.getTagList(LOADED_PROJECTILES, 10);
            System.out.println("loaded: " + loaded);
            loaded.appendTag(projectile);

            main.setTag(LOADED_PROJECTILES, loaded);
//            main.setTag(ATTACHABLE, attach);
            //System.out.println("loaded: " + loaded);
            this.tagCompound.setTag(MELEE_MAG, main);
            System.out.println("main: " + main.toString());
            System.out.println("stack: " + itemStack.getTagCompound());
        }
    }

    public NBTTagList getLoaded() {
        return getMeleeMagOrCreate().getTagList(LOADED_PROJECTILES, 10);
    }

    public NBTTagCompound getLastProjectileCompound() {
        if (!getMeleeMagOrCreate().hasKey(LOADED_PROJECTILES)) return null;
        NBTTagList projectiles = getMeleeMagOrCreate().getTagList(LOADED_PROJECTILES, 10);
        return (NBTTagCompound) projectiles.get(projectiles.tagCount()-1);
    }
}
