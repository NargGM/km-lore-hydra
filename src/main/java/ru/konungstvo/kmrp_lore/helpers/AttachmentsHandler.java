package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.network.play.server.SPacketBlockChange;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.konungstvo.kmrp_lore.command.LoreException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static javax.swing.UIManager.get;
import static net.minecraft.item.Item.getIdFromItem;

public class AttachmentsHandler extends WeaponTagsHandler {
    public static final String ATTACHMENTS = "attachments";
    public static final String ATTACHMENTPOINT = "attachmentPoint";
    public static final String ATTACHABLE = "attachable";
    public static final String PROJECTILE = "projectile";
    public static final String LOADED_PROJECTILES = "loadedProjectilesList";

    public AttachmentsHandler(ItemStack itemStack) {
        super(itemStack);
//        if (itemStack.getTagCompound().getCompoundTag("module").getString("type").contains("магазин")) {
//            NBTTagCompound loaded = new NBTTagCompound();
//            itemStack.getTagCompound().getCompoundTag(ATTACHABLE).merge();
//
//        }
    }

    public static boolean isModule(ItemStack itemStack) {
        if (!itemStack.hasTagCompound()) return false;
        return (itemStack.getTagCompound().hasKey("module"));
    }

    public ItemStack getAttachableFromInventory(String attachmentName, EntityPlayerMP player) {
        for (ItemStack item : player.inventory.mainInventory) {
            if (item.getDisplayName().equals(attachmentName)) {
                return item;
            }
        }
        return null;

    }

    public ArrayList<String> getAttachablesListFromInventory(EntityPlayerMP player) {
        String attachmentPointClass = getAttachmentPointClass();
        return getAttachablesListFromInventory(attachmentPointClass, player);
    }

    public ArrayList<String> getAttachablesListFromInventory(String attachmentPointClass, EntityPlayerMP player) {
        ArrayList<String> attachables = new ArrayList<>();
        for (ItemStack item : player.inventory.mainInventory) {
            if (isAttachableTo(item, attachmentPointClass)) {
                String attachable = item.getDisplayName();
                attachables.add(attachable);
            }
        }
        return attachables;
    }

    public boolean isAttachable(ItemStack attachable) {
        if (attachable.getTagCompound() == null) {
            return false;
        }
        return attachable.getTagCompound().hasKey(ATTACHABLE);
    }

    public void isAttachableTo(ItemStack attachable) {
        String attachmentPointClass = getAttachmentPointClass();
        //System.out.println("attachmentPointClass: " + attachmentPointClass);
        isAttachableTo(attachable, attachmentPointClass);
    }


    public boolean isAttachableTo(ItemStack attachable, String attachmentPointClass) {
        NBTTagList attachableTo = new NBTTagList();
        if (attachable.getTagCompound() != null) {
            attachableTo = attachable.getTagCompound().getCompoundTag(ATTACHABLE).getTagList("attachableTo", 8);
        }
        for (NBTBase base : attachableTo) {
            String str = base.toString().substring(1, base.toString().length()-1);
            //System.out.println("base: " + str);
            //System.out.println("attachmentPointClass: " + attachmentPointClass);
            if (str.equals(attachmentPointClass)) return true;
        }
        return false;
    }

    public boolean isAttachmentPoint() {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        return itemStack.getTagCompound().hasKey(ATTACHMENTPOINT);
    }

    public String getAttachmentPointClass() {
        if (!isAttachmentPoint()) return null;
        if (itemStack.getTagCompound() != null) {
            return itemStack.getTagCompound().getCompoundTag(ATTACHMENTPOINT).getString("attachmentPointClass");
        }
        return null;
    }

    public void attach(ItemStack attachmentPoint, String attachableName, EntityPlayerMP player) {
        ItemStack attachable = getAttachableFromInventory(attachableName, player);
        AttachmentsHandler attachableHandler = new AttachmentsHandler(attachable);

        String attachableSlot = attachableHandler.getAttachableSlot();

        if (!isAttachableTo(attachable, getAttachmentPointClass())) return;

        if (!hasAttachmentSlot(attachableSlot)) return;

        WeaponTagsHandler attachmentWeaponTagsHandler = new WeaponTagsHandler(attachable);
        NBTTagCompound attachableWeaponTags = attachmentWeaponTagsHandler.getWeaponTags();

        WeaponTagsHandler attachmentPointWeaponTagsHandler = new WeaponTagsHandler(attachmentPoint);
        attachmentPointWeaponTagsHandler.addWeaponTag(attachableWeaponTags);

        NBTTagCompound attachments = getAttachmentsOrCreate();

        NBTTagCompound attachableNBT = new NBTTagCompound();

        //ItemLoreHandler attachableItemLoreHandler = new ItemLoreHandler(attachable);
        //NBTTagCompound display = attachableItemLoreHandler.getDisplayOrCreate();

        //GmLoreHandler attachableGmLoreHandler = new GmLoreHandler(attachable);
        //NBTTagList gmlore = attachableGmLoreHandler.getGmLoreOrCreate();

        //NBTTagCompound attachableFallBackNBT = attachableHandler.getAttachableOrCreate();

        int attachableItemID = getIdFromItem(attachable.getItem());
        NBTBase attachableItem = new NBTTagInt(attachableItemID);

        attachableNBT.setTag("itemid", attachableItem);
        if (attachable.getTagCompound() != null) {
            attachableNBT.setTag("tagCompound", attachable.getTagCompound());
        }
        //attachableNBT.setTag("display", display);
        //attachableNBT.setTag("weapontags", attachableWeaponTags);
        //attachableNBT.setTag("gmlore", gmlore);
        //attachableNBT.setTag("attachable", attachableFallBackNBT);

        NBTTagCompound attachmentNBT = new NBTTagCompound();
        attachmentNBT.setTag(attachableName, attachableNBT);

        attachments.getCompoundTag("slots").setTag(attachableSlot, attachmentNBT);
        
        attachable.setCount(0);
    }

    public void detach(ItemStack attachmentPoint, String attachmentName, EntityPlayerMP player) throws LoreException {
        NBTTagCompound attachments = getAttachmentsOrCreate();
        NBTTagCompound attachmentsSlots = attachments.getCompoundTag("slots");
        Set<String> slots = attachments.getCompoundTag("slots").getKeySet();


        NBTTagCompound attachment = null;
        String attachmentSlot = "";
        for (String slot : slots) {
            if (attachmentsSlots.getCompoundTag(slot).hasKey(attachmentName)) {
                attachment = attachmentsSlots.getCompoundTag(slot).getCompoundTag(attachmentName);
                attachmentSlot = slot;
            }

        }
        if (attachment == null) return;

        Item item = Item.getItemById(attachment.getInteger("itemid"));
        ItemStack attachmentItemStack = new ItemStack(item);

        attachmentsSlots.removeTag(attachmentSlot);
        attachmentsSlots.setString(attachmentSlot, "");

        WeaponTagsHandler attachmentPointWeaponTagsHandler = new WeaponTagsHandler(attachmentPoint);
        attachmentItemStack.setTagCompound(attachment.getCompoundTag("tagCompound"));
        WeaponTagsHandler attachmentWeaponTagsHandler = new WeaponTagsHandler(attachmentItemStack);
        Set<String> attachmentWeaponTags = attachment.getCompoundTag("weapontags").getKeySet();
        for (String weapon : attachmentWeaponTags) {
            NBTTagCompound weaponFromAttachmentPoint = attachmentPointWeaponTagsHandler.getWeaponByName(weapon);
            attachmentWeaponTagsHandler.delWeaponTag(weapon);
            attachmentWeaponTagsHandler.addWeaponTagByName(weapon, weaponFromAttachmentPoint);
            attachmentPointWeaponTagsHandler.delWeaponTag(weapon);
        }

        //Item attachmentPointItem = attachmentPoint.getItem();
        //attachmentPointItem.getItemById(1);

        player.inventory.addItemStackToInventory(attachmentItemStack);

    }


    public NBTTagCompound getAttachmentsOrCreate() {
        if (tagCompound.hasKey(ATTACHMENTS)) {
            return tagCompound.getCompoundTag(ATTACHMENTS);
        }
        NBTTagCompound attachments = new NBTTagCompound();
        this.tagCompound.setTag(ATTACHMENTS, attachments);
        return attachments;
    }

    public boolean hasAttachmentSlot(String slot) {
        NBTTagCompound attachments = getAttachmentsOrCreate();
        if (attachments.hasKey("slots")) {
            if (attachments.getCompoundTag("slots").hasKey(slot)) {
                return attachments.getCompoundTag("slots").hasKey(slot, 8);
            }
        }
        return false;
    }

    public NBTTagCompound getAttachmentBySlot(String slot) {
        NBTTagCompound attachments = getAttachmentsOrCreate();
        return attachments.getCompoundTag("slots").getCompoundTag(slot);
    }

    public NBTTagCompound getAttachmentContainsBySlot(String slot) {
        NBTTagCompound attachments = getAttachmentsOrCreate();
        Set<String> slotContains = attachments.getCompoundTag("slots").getCompoundTag(slot).getKeySet();
        for (String contains : slotContains) {
            return attachments.getCompoundTag("slots").getCompoundTag(slot).getCompoundTag(contains);
        }
        return null;
    }

    // FOR MODULE LORE!
    @Override
    public String getCaliber() {
        if (!isMagazine(this.itemStack)) return null;
        String caliber = this.itemStack.getTagCompound().getCompoundTag("module").getString("caliber");
        //System.out.println("caliber: " + caliber);
        return caliber;
    }

    @Override
    public boolean doesProjectileFit(NBTTagCompound projectile) {
        return getCaliber().equals(projectile.getString("caliber"));
    }

    @Override
    public boolean isFull() {
        int capacity = 0;
        capacity = itemStack.getTagCompound().getCompoundTag("module").getInteger("capacity");
        //System.out.println("capacity: " + capacity);
        return itemStack.getTagCompound().getTagList(LOADED_PROJECTILES, 10).tagCount() >= capacity;
    }

    public void loadProjectile(NBTTagCompound projectile, String itemID) {
        projectile = projectile.copy();
        projectile.setString("itemrl", itemID);
        if (itemStack.getTagCompound() != null) {
            NBTTagCompound main = itemStack.getTagCompound();
            //NBTTagCompound attach = main.getCompoundTag(ATTACHABLE);
            NBTTagList loaded = main.getTagList(LOADED_PROJECTILES, 10);
            System.out.println("loaded: " + loaded);
            loaded.appendTag(projectile);

            main.setTag(LOADED_PROJECTILES, loaded);
//            main.setTag(ATTACHABLE, attach);
            //System.out.println("loaded: " + loaded);
            this.itemStack.setTagCompound(main);
            System.out.println("main: " + main.toString());
            System.out.println("stack: " + itemStack.getTagCompound());
        }
    }


    public NBTTagList getLoadedStuff() {
//        return this.itemStack.getTagCompound().getCompoundTag(ATTACHABLE).getTagList(LOADED_PROJECTILES, 10);
        return this.itemStack.getTagCompound().getTagList(LOADED_PROJECTILES, 10);
    }

    public int getMagazineSize() {
        return this.itemStack.getTagCompound().getCompoundTag("module").getInteger("capacity");
    }
    public String getMagazineCaliber() {
        return this.itemStack.getTagCompound().getCompoundTag("module").getString("caliber");
    }

    public int getMod() {
        return this.itemStack.getTagCompound().getCompoundTag("module").getInteger("mod");
    }

    public void setAttachments(NBTTagCompound attachmentsNBT) {
        NBTTagCompound attachments = getAttachmentsOrCreate();
        attachments.merge(attachmentsNBT);
    }

    public NBTTagCompound getAttachmentPointOrCreate() {
        if (tagCompound.hasKey(ATTACHMENTPOINT)) {
            return tagCompound.getCompoundTag(ATTACHMENTPOINT);
        }
        NBTTagCompound attachmentPoint = new NBTTagCompound();
        this.tagCompound.setTag(ATTACHMENTPOINT, attachmentPoint);
        return attachmentPoint;
    }

    public void setAttachmentPoint(NBTTagCompound attachmentPointNBT) {
        NBTTagCompound attachmentPoint = getAttachmentPointOrCreate();
        attachmentPoint.merge(attachmentPointNBT);
    }

    public NBTTagCompound getAttachableOrCreate() {
        if (tagCompound.hasKey(ATTACHABLE)) {
            return tagCompound.getCompoundTag(ATTACHABLE);
        }
        NBTTagCompound attachable = new NBTTagCompound();
        this.tagCompound.setTag(ATTACHABLE, attachable);
        return attachable;
    }

    public String getAttachableSlot() {
        NBTTagCompound attachable = getAttachableOrCreate();
        return attachable.getString("slot");
    }

    public String getAttachmentSlot(NBTTagCompound attachment) {
        return attachment.getString("slot");
    }

    public void setAttachable(NBTTagCompound attachableNBT) {
        NBTTagCompound attachable = getAttachableOrCreate();
        attachable.merge(attachableNBT);
    }

    // ПАТРОНЫ
    public NBTTagCompound getProjectileOrCreate() {
        if (tagCompound.hasKey(PROJECTILE)) {
            return tagCompound.getCompoundTag(PROJECTILE);
        }
        NBTTagCompound projectile = new NBTTagCompound();
        this.tagCompound.setTag(PROJECTILE, projectile);
        return projectile;
    }

    public void setProjectile(NBTTagCompound projectile) {
        NBTTagCompound attachable = getProjectileOrCreate();
        attachable.merge(projectile);
    }

    public static boolean isProjectile(ItemStack stack) {
        if (stack.getTagCompound() == null) return false;
        return stack.getTagCompound().hasKey(PROJECTILE);
    }


    public NBTTagCompound getLastProjectileCompound() {
        if (!tagCompound.hasKey(LOADED_PROJECTILES)) return null;
        NBTTagList projectiles = tagCompound.getTagList(LOADED_PROJECTILES, 10);
        return (NBTTagCompound) projectiles.get(projectiles.tagCount()-1);
    }

    public void popLastProjectile() {
        if (!tagCompound.hasKey(LOADED_PROJECTILES)) return;
        NBTTagList projectiles = tagCompound.getTagList(LOADED_PROJECTILES, 10);
        projectiles.removeTag(projectiles.tagCount()-1);
    }

    @Deprecated
    public boolean isClip (ItemStack clip) {
        if (clip.getTagCompound() == null) {
            return false;
        }
        return clip.getTagCompound().getCompoundTag(ATTACHABLE).getString("slot").equals("clip");
    }

    @Deprecated
    public boolean doesProjectileFitTheClip (ItemStack clip, String projectileClass) {
        NBTTagList clipClass = new NBTTagList();
        if (clip.getTagCompound() != null) {
            clipClass = clip.getTagCompound().getCompoundTag(ATTACHABLE).getTagList("attachableTo", 8);
        }
        for (NBTBase base : clipClass) {
            String str = base.toString().substring(1, base.toString().length()-1);
            System.out.println("base: " + str);
            System.out.println("projectileClass: " + projectileClass);
            if (str.equals(projectileClass)) return true;
        }
        return false;
    }

    @Deprecated
    public boolean loadedClipHasProjectiles() {
        NBTTagCompound clip = getAttachmentContainsBySlot("clip");
        NBTTagList loadedProjectilesList = clip.getCompoundTag("tagCompound").getCompoundTag(ATTACHABLE).
                getTagList(LOADED_PROJECTILES, 10);
        return loadedProjectilesList.tagCount() > 0;
    }

    @Deprecated
    public boolean isWeaponFullyLoaded(String weaponTagName) {
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getWeaponByName(weaponTagName);

        NBTTagList loadedProjectilesList = weapon.getCompoundTag("rangedFirearm").getTagList(LOADED_PROJECTILES, 10);
        int projectilesSlots = weapon.getCompoundTag("rangedFirearm").getInteger("projectilesSlots");
        return loadedProjectilesList.tagCount() == projectilesSlots;
    }

    @Deprecated
    public boolean isWeaponLoaded(String weaponTagName) {
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getWeaponByName(weaponTagName);

        NBTTagList loadedProjectilesList = weapon.getCompoundTag("rangedFirearm").getTagList(LOADED_PROJECTILES, 10);
        return loadedProjectilesList.tagCount() > 0;
    }

    @Deprecated
    public void loadProjectileToClip(String projectileName, EntityPlayerMP player) {
        if (!isClip(itemStack)) return;


        ItemStack projectileItemStack = getAttachableFromInventory(projectileName, player);
        int projectilesItemStackCount = projectileItemStack.getCount();
        NBTTagCompound projectile = null;
        if (projectileItemStack.getTagCompound() != null) {
            projectile = projectileItemStack.getTagCompound().getCompoundTag("projectile");
        } else return;

        if (!doesProjectileFitTheClip(itemStack, projectile.getString("projectileClass"))) return;

        int projectileItemID = getIdFromItem(projectileItemStack.getItem());
        NBTBase projectileItem = new NBTTagInt(projectileItemID);
        NBTTagCompound projectileNBT = new NBTTagCompound();
        projectileNBT.setTag("itemid", projectileItem);
        if (projectileItemStack.getTagCompound() != null) {
            projectileNBT.setTag("tagCompound", projectileItemStack.getTagCompound());
        }

        NBTTagCompound loadedProjectileNBT = new NBTTagCompound();
        loadedProjectileNBT.setTag(projectileName, projectileNBT);
        System.out.println("projectile: " + loadedProjectileNBT.toString());
        //NBTBase projectileString = new NBTTagString(loadedProjectileNBT.toString());
        if (itemStack.getTagCompound() != null) {
            itemStack.getTagCompound().getCompoundTag(ATTACHABLE).getTagList(LOADED_PROJECTILES, 10).appendTag(loadedProjectileNBT);
        }
        projectileItemStack.setCount(projectilesItemStackCount-1);


    }

    @Deprecated
    public void loadProjectileFromClip(String weaponTagName) {
        if (isWeaponFullyLoaded(weaponTagName)) return;
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getWeaponByName(weaponTagName);

        NBTTagCompound clip = getAttachmentContainsBySlot("clip");
        NBTTagList loadedProjectilesList = clip.getCompoundTag("tagCompound").getCompoundTag(ATTACHABLE).
                getTagList(LOADED_PROJECTILES, 10);
        int projectilesCount = loadedProjectilesList.tagCount();
        NBTTagCompound projectile = loadedProjectilesList.getCompoundTagAt(projectilesCount-1);
        loadedProjectilesList.removeTag(projectilesCount-1);
        weapon.getCompoundTag("rangedFirearm").getTagList(LOADED_PROJECTILES, 10).appendTag(projectile);
        System.out.println("projectile loaded: " + projectile.toString());
    }

    @Deprecated
    public void fireProjectile(String weaponTagName) {
        if (!isWeaponLoaded(weaponTagName)) return;
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapon = weaponTagsHandler.getWeaponByName(weaponTagName);

        NBTTagList loadedProjectilesList = weapon.getCompoundTag("rangedFirearm").getTagList(LOADED_PROJECTILES, 10);
        NBTTagCompound projectile = loadedProjectilesList.getCompoundTagAt(0);
        loadedProjectilesList.removeTag(0);
        System.out.println("projectile fired: " + projectile.toString());
        if (loadedClipHasProjectiles()) loadProjectileFromClip(weaponTagName);
    }

}


