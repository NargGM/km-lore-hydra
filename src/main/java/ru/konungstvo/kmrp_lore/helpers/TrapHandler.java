package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class TrapHandler extends LoreHandler {
    public static final String TRAP = "traptag";

    public TrapHandler(ItemStack itemStack) {
        super(itemStack);
    }
    public String getTrapString() {
        if(itemStack.getTagCompound() == null) return "";
        if(!itemStack.getTagCompound().hasKey(TRAP)) return "";
        NBTTagCompound trap = itemStack.getTagCompound().getCompoundTag(TRAP);
        return trap.getInteger("trapdamage") + "";
    }

    public NBTTagCompound getTrapTagOrCreate() {
        if (tagCompound.hasKey(TRAP)) {
            return tagCompound.getCompoundTag(TRAP);
        }
        NBTTagCompound trapTags = new NBTTagCompound();
        this.tagCompound.setTag(TRAP, trapTags);
        return trapTags;
    }

    public void setTrap(int trapdamage) {
        NBTTagCompound trapCompound = getTrapTagOrCreate();
        trapCompound.setInteger("trapdamage", trapdamage);
    }


    public void removeTrap() {
        tagCompound.removeTag(TRAP);
    }
}
