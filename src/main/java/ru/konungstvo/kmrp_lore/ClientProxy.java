package ru.konungstvo.kmrp_lore;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.command.GmTagCommand;
import ru.konungstvo.kmrp_lore.helpers.*;
import ru.konungstvo.kmrp_lore.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.network.PacketMessage;
import ru.konungstvo.kmrp_lore.network.PacketMessageHandler;
import ru.konungstvo.kmrp_lore.network.currentDayMessage;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.konungstvo.kmrp_lore.helpers.ExpireDate.hasExpiration;
import static ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler.DURABILITY_DICT_COMPOUND;
import static ru.konungstvo.kmrp_lore.helpers.WeightHandler.WEIGHT;

public class ClientProxy extends CommonProxy {
    public static NBTTagCompound copiedLore = null;
    public static final List<String> excludedStrings = Arrays.asList(
            "Когда",
            "Урон",
            "Скорость"
    );

    private static HashMap<Integer, String> skillTable = new HashMap<>();

    static private ItemStack lastItemStack = null;

    public static int currentDay = -666;


    public ClientProxy() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(this);
        System.out.println("Registered packet handler");
        ClientCommandHandler.instance.registerCommand(new GmTagCommand());
        KMPacketHandler.INSTANCE.registerMessage(PacketMessageHandler.class, PacketMessage.class, 1, Side.SERVER);
        KMPacketHandler.INSTANCE.registerMessage(currentDayMessage.currentDayMessageHandler.class, currentDayMessage.class, 2, Side.CLIENT);
        PermissionAPI.registerNode("km.gm", DefaultPermissionLevel.OP, "Game Master permission");

        skillTable.put(-5, "абсолютно ублюдски");
        skillTable.put(-4, "ужасно---");
        skillTable.put(-3, "ужасно--");
        skillTable.put(-2, "ужасно-");
        skillTable.put(-1, "ужасно");
        skillTable.put(0, "плохо");
        skillTable.put(1, "посредственно");
        skillTable.put(2, "нормально");
        skillTable.put(3, "хорошо");
        skillTable.put(4, "отлично");
        skillTable.put(5, "превосходно");
        skillTable.put(6, "легендарно");
        skillTable.put(7, "легендарно+");
        skillTable.put(8, "легендарно++");
        skillTable.put(9, "легендарно+++");
        skillTable.put(10, "КАК АЛЛАХ");

    }

    public String getResultAsString(int level) {
        //return DataHolder.getInstance().getSkillTable().get(result) + " [" + DataHolder.getInstance().getSkillTable().get(firstResult) + "]";
        return skillTable.get(level);
    }

    @SubscribeEvent
    public void tooltip_event(ItemTooltipEvent event) {
        List<String> toRemove = new ArrayList<>();
        for (String str : event.getToolTip()) {
            if (str.isEmpty()) {
                toRemove.add(str);
                continue;
            }
            for (String ex : excludedStrings) {
                if (str.contains(ex)) {
                    toRemove.add(str);
                }
            }
        }

        if (WeaponTagsHandler.hasWeaponTags(event.getItemStack())) {
            event.getToolTip().removeAll(toRemove);
            boolean sneak = GuiScreen.isShiftKeyDown();
            WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(event.getItemStack());

            NBTTagCompound weaponTags = weaponTagsHandler.getWeaponTags();

            event.getToolTip().add("");
            event.getToolTip().add(TextFormatting.RED + "Теги оружия");
            weaponTagsHandler.setDefaultTagIfNone();
            // CREATE WEAPON OVERVIEW FOR TOOLTIP
            for (String key : weaponTags.getKeySet()) {
                if (key.equals("commonDurability")) continue;
                if (key.equals(WeaponTagsHandler.DEFAULT_WEAPON)) continue;

                TextFormatting color = TextFormatting.DARK_AQUA;
                if (weaponTagsHandler.isWeaponDefault(key)) color = TextFormatting.AQUA;

                NBTTagCompound weapon = weaponTags.getCompoundTag(key);

                int slot = -1;
                String modname = "";
                if (weapon.hasKey("attachment")) {
                    slot = weapon.getInteger("attachment");
                    NBTTagCompound attach = weaponTagsHandler.getAttachmentBySlot(slot);
                    if (event.getItemStack().getTagCompound() != null) {
                        if (attach.hasKey("display")) {
                            if (attach.getCompoundTag("display").hasKey("Name")) {
                                modname = attach.getCompoundTag("display").getString("Name");
                            }
                        }
                    }
                }
                // MELEE
                if (weapon.hasKey(WeaponTagsHandler.MELEE_KEY)) {

                    NBTTagCompound meleeStats = weapon.getCompoundTag(WeaponTagsHandler.MELEE_KEY);

                    // BASIC INFO
                    String toTooltip = TextFormatting.GRAY + "[Ближнее";
                    String category = meleeStats.getString("category");


                    // DAMAGE
                    int damage = weaponTagsHandler.getWeaponMod(key);
                    String damageStr = String.valueOf(damage);
                    if (damage > 0) damageStr = "+" + damageStr;
                    String damageStrToTooltip = TextFormatting.DARK_RED + "[" + damageStr + "]";
                    String reachStrToTooltip = TextFormatting.BLUE + "[" + meleeStats.getInteger("reach") + "] " + TextFormatting.RED;


                    // FACTORS
                    NBTTagList factorsList = meleeStats.getTagList("factors", 8);
                    String factors = "";
                    for (int i = 0; i < factorsList.tagCount(); i++) {
                        factors += factorsList.getStringTagAt(i);
                        if (i != factorsList.tagCount() - 1) factors += ", ";
                    }

                    event.getToolTip().add(TextFormatting.DARK_AQUA + "[" + color + key + TextFormatting.DARK_AQUA + "] " +
                            damageStrToTooltip + " " + reachStrToTooltip);
                    if(!modname.equals("")) {
                        event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + modname + TextFormatting.DARK_GRAY + "]");
                    } else if (slot != -1) {
                        event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + slot + TextFormatting.DARK_GRAY + "]");
                    }
                    event.getToolTip().add(toTooltip + ", " + category + ", " + factors + "]");
                    //event.getToolTip().add(damageStrToTooltip + " " + rangeStr);
//                    event.getToolTip().add(TextFormatting.GRAY + "[" + factors + "]");
                    if (meleeStats.hasKey("modsplus")) {
                        String modsplus = meleeStats.getString("modsplus");
                        if (!modsplus.isEmpty()) {
                            event.getToolTip().add("§6Дополнительные эффекты:");
                            event.getToolTip().add("[" + modsplus + "]");
                        }
                    }

                    if(meleeStats.hasKey("diff")) event.getToolTip().add("§eСложность: " + getResultAsString(meleeStats.getInteger("diff")));
                    if(meleeStats.hasKey("cost")) {
                        if(meleeStats.getInteger("cost") > 0) event.getToolTip().add("§bСтоимость в потенциале: " + meleeStats.getInteger("cost"));
                    }
                    if(meleeStats.hasKey("needAmmo") && meleeStats.getBoolean("needAmmo")) {
                        event.getToolTip().add(TextFormatting.GRAY + "Использует расходники на атаку.");
                    }
                    if (meleeStats.hasKey("energy")) {
                        event.getToolTip().add(TextFormatting.GRAY + "Расход энергии: " + meleeStats.getInteger("energy"));
                    }

                }

                // RANGED FIREARM

                if (weapon.hasKey(WeaponTagsHandler.FIREARM_KEY)) {

                    NBTTagCompound firearmStats = weapon.getCompoundTag(WeaponTagsHandler.FIREARM_KEY);

                    // BASIC INFO
                    String name = key;
                    if (name.isEmpty()) {
                        name = "Огнестрел";
                    }
                    String toTooltip = color + "[" + name + "]";
                    String type = firearmStats.getString("type");
                    String sort = firearmStats.getString("sort");
                    String caliber = firearmStats.getString("caliber");
                    Integer capacity = firearmStats.getInteger("capacity");
                    String range = firearmStats.getString("range");
                    boolean flamer = firearmStats.hasKey("flamerstats");
                    boolean grenade = firearmStats.hasKey("grenadestats");
                    String reload = String.valueOf(firearmStats.getInteger("reload"));
                    boolean serial = (firearmStats.hasKey("serial") && firearmStats.getBoolean("serial"));
                    boolean sdouble = (firearmStats.hasKey("double") && firearmStats.getBoolean("double"));
                    int energy = -666;
                    int energyDamage = -666;
                    String energyType = "";
                    if (firearmStats.hasKey("energy")) energy = firearmStats.getInteger("energy");
                    if (firearmStats.hasKey("energyDamage")) energyDamage = firearmStats.getInteger("energyDamage");
                    if (firearmStats.hasKey("energyType")) energyType = firearmStats.getString("energyType");
                    int homemade = 0;
                    if (firearmStats.hasKey("homemade")) homemade = firearmStats.getInteger("homemade");
                    switch (sort) {
                        case "1":
                            sort = "Лёгкий";
                            if (type.contains("интовка")) sort = "Лёгкая";
                            break;
                        case "2":
                            sort = "Стандартный";
                            if (type.contains("интовка")) sort = "Стандартная";
                            break;
                        case "3":
                            sort = "Тяжелый";
                            if (type.contains("интовка")) sort = "Тяжёлая";
                            break;
                        default:
                            sort = "";
                            break;
                    }
                    if (!energyType.isEmpty()) {
                        if (energyType.equals("laser")) {
                            if (type.contains("интовка")) sort += " лазерная";
                            else sort += " лазерный";
                        }
                        if (energyType.equals("plasma")) {
                            if (type.contains("интовка")) sort += " плазменная";
                            else sort += " плазменный";
                        }
                    }
                    if (firearmStats.hasKey("sProj")) {
                        event.getToolTip().add(toTooltip);
                        if (!modname.equals("")) {
                            event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + modname + TextFormatting.DARK_GRAY + "]");
                        } else if (slot != -1) {
                            event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + slot + TextFormatting.DARK_GRAY + "]");
                        }
                        event.getToolTip().add(TextFormatting.GRAY + type);
                        //event.getToolTip().add(TextFormatting.GRAY + type + ", " + caliber);
                        if (!flamer && !grenade) event.getToolTip().add(TextFormatting.GRAY + "Дальность: " + range);
                        if (serial) {
                            event.getToolTip().add(TextFormatting.GRAY + "Есть автоматический огонь");
                        }
                        if (sdouble) {
                            event.getToolTip().add(TextFormatting.GRAY + "Доступен двойной выстрел");
                        }
                        if (!grenade) {
                            for (NBTBase ammo : firearmStats.getTagList("sProj", 10)) {
                                NBTTagCompound tagCompound = (NBTTagCompound) ammo;
                                event.getToolTip().add(ammoToString(tagCompound.getTag("projectile")));
                                NBTTagCompound proje = (NBTTagCompound) tagCompound.getTag("projectile");
                                if (proje.hasKey("modsplus")) {
                                    String modsplus = proje.getString("modsplus");
                                    if (!modsplus.isEmpty()) event.getToolTip().add("§6Дополнительные эффекты:");
                                    if (!modsplus.isEmpty()) event.getToolTip().add("§6[" + modsplus + "]");
                                }
                                if (proje.hasKey("diff"))
                                    event.getToolTip().add("§eСложность: " + getResultAsString(proje.getInteger("diff")));
                                if (proje.hasKey("cost")) {
                                    if (proje.getInteger("cost") > 0)
                                        event.getToolTip().add("§bСтоимость в потенциале: " + proje.getInteger("cost"));
                                }
                            }
                        }

                        if (flamer) {
                            event.getToolTip().add(TextFormatting.GRAY + "[Режимы огня]");
                            NBTTagCompound flamerstats = firearmStats.getCompoundTag("flamerstats");
                            for (String flamermod : flamerstats.getKeySet()) {
                                NBTTagCompound flamerstat = flamerstats.getCompoundTag(flamermod);
                                event.getToolTip().add(TextFormatting.GOLD + "["+flamermod+"]");
                                event.getToolTip().add(TextFormatting.GRAY + "Дальность: " + flamerstat.getInteger("distance"));
                                event.getToolTip().add(TextFormatting.GRAY + "Угол: " + flamerstat.getInteger("angle"));
                                event.getToolTip().add(TextFormatting.GRAY + "Расход топлива: " + flamerstat.getInteger("fuel"));
                                event.getToolTip().add(TextFormatting.GRAY + "Урон прочности: " + flamerstat.getInteger("durdamage"));
                            }
                        }

                        if (grenade) {
                            event.getToolTip().add(TextFormatting.GRAY + "[Характеристики снаряда]");
                            NBTTagCompound grenadestats = firearmStats.getCompoundTag("grenadestats");
                            event.getToolTip().add(TextFormatting.GOLD + "[" + grenadestats.getString("type") + "]");
                            if(grenadestats.hasKey("defendable") && !grenadestats.getBoolean("defendable")) event.getToolTip().add(TextFormatting.GOLD + "Нельзя защищаться");
                            if(grenadestats.hasKey("ignoresarmor") && grenadestats.getBoolean("ignoresarmor")) event.getToolTip().add(TextFormatting.GOLD + "Игнорирует броню");
                            if(grenadestats.getBoolean("ricochet")) event.getToolTip().add(TextFormatting.GOLD + "Рикошет");
                            event.getToolTip().add(TextFormatting.GRAY + "Тип: " + grenadestats.getString("boomType"));
                            event.getToolTip().add(TextFormatting.GRAY + "Урон/радиус: " + grenadestats.getInteger("damage"));
                            event.getToolTip().add(TextFormatting.GRAY + "Шаг: " + grenadestats.getInteger("distance"));
                            event.getToolTip().add(TextFormatting.GRAY + "Шаг наклона: " + grenadestats.getInteger("range"));
                            event.getToolTip().add(TextFormatting.GRAY + "Падение наклона: " + grenadestats.getInteger("loss"));
                            event.getToolTip().add(TextFormatting.GRAY + "Угол наклона: " + grenadestats.getInteger("angle"));
                            event.getToolTip().add(TextFormatting.GRAY + "Максимальная дальность: " + grenadestats.getInteger("maxrange"));
                        }

                    } else {
                        event.getToolTip().add(toTooltip);
                        if(!modname.equals("")) {
                            event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + modname + TextFormatting.DARK_GRAY + "]");
                        } else if (slot != -1) {
                            event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + slot + TextFormatting.DARK_GRAY + "]");
                        }
                        event.getToolTip().add(TextFormatting.GRAY + sort
                                    + " " + type + ", калибр " + caliber);
                        if (energyDamage != -666) {
                            event.getToolTip().add(TextFormatting.GRAY + "Урон: " + energyDamage);
                        }
                        if (energy != -666) {
                            event.getToolTip().add(TextFormatting.GRAY + "Расход энергии: " + energy);
                        }
                        if (!flamer && !grenade) event.getToolTip().add(TextFormatting.GRAY + "Дальность: " + range);
                        if (!reload.equals("0")) {
                            event.getToolTip().add(TextFormatting.GRAY + reload + " " + (reload.equals("1") ? "ход":"хода") + " на смену магазина");
                        }
                        if (serial) {
                            event.getToolTip().add(TextFormatting.GRAY + "Есть автоматический огонь");
                        }
                        if (sdouble) {
                            event.getToolTip().add(TextFormatting.GRAY + "Доступен двойной выстрел");
                        }

                        if (homemade > 0) {
                            event.getToolTip().add(TextFormatting.DARK_RED + "Шанс заклинить: " + homemade + "%");
                        }

                        if (flamer) {
                            event.getToolTip().add(TextFormatting.GRAY + "[Режимы огня]");
                            NBTTagCompound flamerstats = firearmStats.getCompoundTag("flamerstats");
                            for (String flamermod : flamerstats.getKeySet()) {
                                NBTTagCompound flamerstat = flamerstats.getCompoundTag(flamermod);
                                event.getToolTip().add(TextFormatting.GOLD + "["+flamermod+"]");
                                event.getToolTip().add(TextFormatting.GRAY + "Дальность: " + flamerstat.getInteger("distance"));
                                event.getToolTip().add(TextFormatting.GRAY + "Угол: " + flamerstat.getInteger("angle"));
                                event.getToolTip().add(TextFormatting.GRAY + "Расход топлива: " + flamerstat.getInteger("fuel"));
                                event.getToolTip().add(TextFormatting.GRAY + "Урон прочности: " + flamerstat.getInteger("durdamage"));
                            }
                        }

                        if (grenade) {
                            event.getToolTip().add(TextFormatting.GRAY + "[Характеристики снаряда]");
                            NBTTagCompound grenadestats = firearmStats.getCompoundTag("grenadestats");
                            event.getToolTip().add(TextFormatting.GOLD + "[" + grenadestats.getString("type") + "]");
                            if(grenadestats.hasKey("defendable") && !grenadestats.getBoolean("defendable")) event.getToolTip().add(TextFormatting.GOLD + "[нельзя защищаться]");
                            if(grenadestats.hasKey("ignoresarmor") && grenadestats.getBoolean("ignoresarmor")) event.getToolTip().add(TextFormatting.GOLD + "[игнорирует броню]");
                            if(grenadestats.getBoolean("ricochet")) event.getToolTip().add(TextFormatting.GOLD + "[рикошет]");
                            event.getToolTip().add(TextFormatting.GRAY + "Тип: " + grenadestats.getString("boomType"));
                            event.getToolTip().add(TextFormatting.GRAY + "Урон/радиус: " + grenadestats.getInteger("damage"));
                            event.getToolTip().add(TextFormatting.GRAY + "Шаг: " + grenadestats.getInteger("distance"));
                            event.getToolTip().add(TextFormatting.GRAY + "Шаг наклона: " + grenadestats.getInteger("range"));
                            event.getToolTip().add(TextFormatting.GRAY + "Падение наклона: " + grenadestats.getInteger("loss"));
                            event.getToolTip().add(TextFormatting.GRAY + "Угол наклона: " + grenadestats.getInteger("angle"));
                            event.getToolTip().add(TextFormatting.GRAY + "Максимальная дальность: " + grenadestats.getInteger("maxrange"));
                        }

                        // LOADED AMMO
                        if (capacity == 0) {
                            NBTTagCompound magazineTag = weaponTagsHandler.getMagazine();
                            if (magazineTag == null || magazineTag.hasNoTags() || magazineTag.toString().equals("{}")) {

                            } else {
                                NBTTagList loadedStuff = magazineTag.getTagList(AttachmentsHandler.LOADED_PROJECTILES, 10);
                                if (loadedStuff.tagCount() > 0) {
                                    for (int i = 0; i < loadedStuff.tagCount(); i++) {
                                        NBTTagCompound ammo = loadedStuff.getCompoundTagAt(i);
                                        event.getToolTip().add(TextFormatting.DARK_RED + "Подключено: " + ammoToString(ammo.getTag("projectile")));
                                    }
                                }
                            }
                        } else if (weaponTagsHandler.isFirearmLoadedByName(key)) {
                            event.getToolTip().add(TextFormatting.RED + "[" + firearmStats.getTagList(WeaponTagsHandler.CHAMBER, 10).tagCount() + "/" + capacity + "] Заряжено:");
                            for (NBTBase ammo : firearmStats.getTagList(WeaponTagsHandler.CHAMBER, 10)) {
                                NBTTagCompound tagCompound = (NBTTagCompound) ammo;
                                event.getToolTip().add(ammoToString(tagCompound.getTag("projectile")));

                            }
                        } else {
                            event.getToolTip().add(TextFormatting.RED + "[" + firearmStats.getTagList(WeaponTagsHandler.CHAMBER, 10).tagCount() + "/" + capacity + "] Разряжено");
                        }



                        int mod = weaponTagsHandler.getModuleMods();
                        if (mod > 0) {
                            event.getToolTip().add("Суммарный бонус: +" + mod);
                        } else if (mod < 0) {
                            event.getToolTip().add("Суммарный штраф: " + mod);
                        }
                    }

                }

                if (weapon.hasKey("statusEffect")) {
                    // MAGIC

                    String name = key;
                    if (name.isEmpty()) {
                        name = "Эффект";
                    }
                    String toTooltip = color + "[" + name + "]";
                    event.getToolTip().add(toTooltip);
                    NBTTagCompound meleeStats = weapon.getCompoundTag(WeaponTagsHandler.STATUS_KEY);
                    event.getToolTip().add("§bЭффект: " + meleeStats.getString("statusType"));
                    event.getToolTip().add("§eПродолжительность: " + meleeStats.getInteger("turns") + meleeStats.getString("statusEnd"));
                    String context = meleeStats.getString("context");
                    int contextInt = meleeStats.getInteger("contextInt");
                    if (!context.isEmpty() || contextInt != -666) {
                        event.getToolTip().add("§7Контекст: " + context + (contextInt != -666 ? (context.isEmpty() ? contextInt : " " + contextInt) : ""));
                    }
                }

                if (weapon.hasKey("magic")) {
                    // MAGIC
                    NBTTagCompound meleeStats = weapon.getCompoundTag(WeaponTagsHandler.MAGIC_KEY);
                    if (meleeStats.hasKey("diff")) event.getToolTip().add("§eСложность: " + getResultAsString(meleeStats.getInteger("diff")));
                    if (meleeStats.hasKey("cost")) {
                        if(meleeStats.getInteger("cost") > 0) event.getToolTip().add("§bСтоимость в потенциале: " + meleeStats.getInteger("cost"));
                    }
                    if (meleeStats.hasKey("turns")) event.getToolTip().add("§bХоды подготовки: " + meleeStats.getInteger("turns"));
                    if (meleeStats.hasKey("debuff")) event.getToolTip().add("§cШтраф при подготовке: " + meleeStats.getInteger("debuff"));
                    if (meleeStats.hasKey("defence")) event.getToolTip().add("§2Защита: " + meleeStats.getString("defence"));
                    if (meleeStats.hasKey("range")) event.getToolTip().add("§7Дальность: " + meleeStats.getInteger("range"));
                    if (meleeStats.hasKey("multicast")) event.getToolTip().add("§9Кол-во целей: до " + meleeStats.getInteger("multicast"));
                    if (meleeStats.hasKey("multicast") && meleeStats.hasKey("chain")) event.getToolTip().add("§eЦепь: " + meleeStats.getInteger("chain") + " метров");
                    if (meleeStats.hasKey("multicast") && meleeStats.hasKey("radius")) event.getToolTip().add("§eРадиус: " + meleeStats.getInteger("radius") + " метров");
                    if (meleeStats.hasKey("instant") && meleeStats.getBoolean("instant")) event.getToolTip().add("§bМгновенное использование");
                    if (meleeStats.hasKey("cooldown")) event.getToolTip().add("§1Восстановление: " + meleeStats.getInteger("cooldown"));
                }

                if (weapon.hasKey("shield")) {
                    // DEFENCE
                    NBTTagCompound shieldStats = weapon.getCompoundTag("shield");
                    int defence = shieldStats.getInteger("defence");
                    String defenceStr = String.valueOf(defence);
                    event.getToolTip().add(color + "[" + key + "] " + TextFormatting.RED + "[+" + defenceStr + "]");
                    event.getToolTip().add(TextFormatting.GRAY + "[" + shieldStats.getString("category") + ", " +
                            "" + shieldStats.getString("material") + "]");

                    //event.getToolTip().add(TextFormatting.RED + "Защита: [" + defenceStr + "]");
                }

                if (weapon.getKeySet().contains("armor")) {
                    ArmorDurabilityHandler armorDurabilityHandler = new ArmorDurabilityHandler(event.getItemStack());
                    event.getToolTip().add("§7[Броня]");
                    NBTTagCompound armorStats = weapon.getCompoundTag("armor");
                    for (String part : armorStats.getKeySet()) {
                        String lore = "";
                        if (armorStats.getCompoundTag(part).hasKey("l")) lore = " §a[Лор] ";

                        event.getToolTip().add("§7[" + part + ": " + armorStats.getCompoundTag(part).getString("t") + "] " + armorDurabilityHandler.countString(part)  + "[П]" + lore);
                        if (sneak) {
                            if (armorStats.getCompoundTag(part).hasKey("n")) event.getToolTip().add(armorStats.getCompoundTag(part).getString("n"));
                            if (armorStats.getCompoundTag(part).hasKey("l")) {
                                NBTTagList nbtTagList = armorStats.getCompoundTag(part).getTagList("l", 8);
                                for (int z = 0; z < nbtTagList.tagCount(); z++) {
                                    event.getToolTip().add(nbtTagList.getStringTagAt(z));
                                }
                            }
                            event.getToolTip().add("§7[§9Вес: " + armorStats.getCompoundTag(part).getDouble("w")  + armorDurabilityHandler.countString(part) + " Прочность: " +  armorStats.getCompoundTag(part).getInteger("cd") + "/" + armorStats.getCompoundTag(part).getInteger("d") + "§7]");
                        }
                    }


                }

                if (weapon.hasKey("proficiency")) {
                    NBTTagList profList = weapon.getTagList("proficiency", 8);
                    String prof = "";
                    for (int i = 0; i < profList.tagCount(); i++) {
                        prof += profList.getStringTagAt(i);
                        if (i != profList.tagCount() - 1) prof += ", ";
                    }
                    event.getToolTip().add(TextFormatting.GRAY + "Умения:");
                    event.getToolTip().add(TextFormatting.GRAY + "[" + prof + "]");

                }

                if (SkillSaverHandler.hasCompound(event.getItemStack())) {
                    SkillSaverHandler ssv = new SkillSaverHandler(event.getItemStack());
                    NBTTagCompound result = ssv.getFullSkillSaverForWeapon(event.getEntityPlayer().getName(), key);

                    for (String string : result.getKeySet()) {
                        event.getToolTip().add(TextFormatting.GRAY + "[" + string + ": " + result.getString(string) + "]");
                    }
                }
                //event.getToolTip().add("");
            }
        }

        if (MeleeMagHandler.hasMeleeMag(event.getItemStack())) {
            MeleeMagHandler meleeMagHandler = new MeleeMagHandler(event.getItemStack());
            if (meleeMagHandler.isLoaded()) {
                event.getToolTip().add(TextFormatting.RED + "[" + meleeMagHandler.getLoaded().tagCount() + "/" + meleeMagHandler.getCapacity() + "] Заряжено:");
                for (NBTBase ammo : meleeMagHandler.getLoaded()) {
                    NBTTagCompound tagCompound = (NBTTagCompound) ammo;
                    event.getToolTip().add(ammoToString(tagCompound.getTag("projectile")));

                }
            } else {
                event.getToolTip().add(TextFormatting.RED + "[" + meleeMagHandler.getLoaded().tagCount() + "/" + meleeMagHandler.getCapacity() + "] Разряжено " + meleeMagHandler.getCaliber());
            }
        }

        // DURABILITY
        boolean durabilitySpace = false;
        WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(event.getItemStack());
        if (weaponDurabilityHandler.hasDurabilityDict()) {
            if (!durabilitySpace) {
                event.getToolTip().add("");
                durabilitySpace = true;
            }

            NBTTagCompound durabilityDict = weaponDurabilityHandler.getDurabilityDict();

            int durability = durabilityDict.getInteger("durability");
            int maxDurability = durabilityDict.getInteger("maxDurability");
            int percentageRatio = weaponDurabilityHandler.getPercentageRatio();
            StringBuilder durString = new StringBuilder(TextFormatting.DARK_GRAY + "[Прочность предмета]");

            String color2 = "";
            if (percentageRatio > 75) {
                color2 = "§2";
                durString.append(" §2[ЦЕЛО]");
            } else if (percentageRatio > 50) {
                color2 = "§e";
                durString.append(" §e[ПОЦАРАПАНО]");
            } else if (percentageRatio > 25) {
                color2 = "§7";
                durString.append(" §7[ПОТРЕПАНО]");
            } else if (percentageRatio > 0) {
                color2 = "§c";
                durString.append(" §c[ИЗНОШЕНО]");
            } else if (percentageRatio == 0) {
                color2 = "§4";
                durString.append(" §4[СЛОМАНО]");
            } else if (percentageRatio < 0) {
                color2 = "§8";
                if (durability < -600) {
                    durString.append(" §8[РАЗОБРАНО НА ДЕТАЛИ]");
                } else {
                    durString.append(" §8[РАЗВАЛИЛОСЬ]");
                }
            }
            if(percentageRatio >= 0) durString.append(" ").append(color2).append("[").append(durability).append("/").append(maxDurability).append("]");
            event.getToolTip().add(durString.toString());
        }

        if(event.getItemStack().hasTagCompound() && event.getItemStack().getTagCompound() != null) {
            if (event.getItemStack().getTagCompound().hasKey("attachments")) {
                for(String attach : event.getItemStack().getTagCompound().getCompoundTag("attachments").getKeySet()) {
                    //System.out.println(attach);
                    NBTTagCompound attachment = event.getItemStack().getTagCompound().getCompoundTag("attachments").getCompoundTag(attach);
                    if (attachment.hasKey(DURABILITY_DICT_COMPOUND)) {
                        //System.out.println(DURABILITY_DICT_COMPOUND);
                        String name = "";
                        NBTTagCompound dur = attachment.getCompoundTag(DURABILITY_DICT_COMPOUND);
                        if (attachment.hasKey("display")) {
                            if (attachment.getCompoundTag("display").hasKey("Name")) {
                                name = attachment.getCompoundTag("display").getString("Name");
                            }
                        }
                        if(name.isEmpty()) name = attach;
                        StringBuilder durString = new StringBuilder(TextFormatting.DARK_GRAY + "[Прочность модуля " + name + TextFormatting.DARK_GRAY + "]");
                        int durability = dur.getInteger("durability");
                        int maxDurability = dur.getInteger("maxDurability");
                        float result = (float) durability / maxDurability;
                        int percentageRatio = Math.round(result * 100);
                        String color2 = "";
                        if (percentageRatio > 75) {
                            color2 = "§2";
                            durString.append(" §2[ЦЕЛО]");
                        } else if (percentageRatio > 50) {
                            color2 = "§e";
                            durString.append(" §e[ПОЦАРАПАНО]");
                        } else if (percentageRatio > 25) {
                            color2 = "§7";
                            durString.append(" §7[ПОТРЕПАНО]");
                        } else if (percentageRatio > 0) {
                            color2 = "§c";
                            durString.append(" §c[ИЗНОШЕНО]");
                        } else if (percentageRatio == 0) {
                            color2 = "§4";
                            durString.append(" §4[СЛОМАНО]");
                        } else if (percentageRatio < 0) {
                            color2 = "§8";
                            if (durability < -600) {
                                durString.append(" §8[РАЗОБРАНО НА ДЕТАЛИ]");
                            } else {
                                durString.append(" §8[РАЗВАЛИЛОСЬ]");
                            }
                        }
                        if(percentageRatio >= 0) durString.append(" ").append(color2).append("[").append(durability).append("/").append(maxDurability).append("]");
                        if (!durabilitySpace) {
                            event.getToolTip().add("");
                            durabilitySpace = true;
                        }
                        event.getToolTip().add(durString.toString());
                    }
                }

            }
        }

        boolean erm = false;

        EquipmentBuffTagHandler equipmentBuffHandler = new EquipmentBuffTagHandler(event.getItemStack());
        if (equipmentBuffHandler.hasEbTag()) {
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            String[] tags = equipmentBuffHandler.getStringBuffs().split("\\[ТУТ ПЕРЕНОС]");
            for (String tag : tags) {
                event.getToolTip().add(tag);
            }
        }

        // WEIGHT
        WeightHandler weightHandler = new WeightHandler(event.getItemStack());
        int weight = weightHandler.getWeightAnything();

        if (weight != 0) {
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            event.getToolTip().add("§9Вес: " + weight);
        }


        int psychweight = weightHandler.getPsychWeight();
        if (psychweight != 0) {
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            event.getToolTip().add("§5Психический вес: " + psychweight);
        }

        if (hasExpiration(event.getItemStack())) {
            ExpireDate expireDate = new ExpireDate(event.getItemStack());
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            if (expireDate.neverExpired()) {
                event.getToolTip().add("§2[Без срока годности]");
                if (expireDate.getNutritionValue() != -666) {
                    event.getToolTip().add("§2[Восстанавливает " + expireDate.getNutritionValue() + " ед. еды]");
                }
            } else {
                if (expireDate.getExpireDate() != -666) {
                    if (currentDay == -666) {
                        event.getToolTip().add("§4[Я ошибка]");
                    } else {
                        if (currentDay >= expireDate.getExpireDate()) {
                            event.getToolTip().add("§4[Пришло в негодность]");
                        } else {
                            event.getToolTip().add("§4[Придёт в негодность через " + (expireDate.getExpireDate() - currentDay) + getEnder(expireDate.getExpireDate() - currentDay) + "]");
                            if (expireDate.getNutritionValue() != -666) {
                                event.getToolTip().add("§2[Восстанавливает " + expireDate.getNutritionValue() + " ед. еды]");
                            }
                        }
                    }
                } else if (event.getEntityPlayer().isCreative()) {
                    event.getToolTip().add("§6[Срок годности " + expireDate.getExpire() + "]");
                    if (expireDate.getNutritionValue() != -666) {
                        event.getToolTip().add("§2[Восстанавливает " + expireDate.getNutritionValue() + " ед. еды]");
                    }
                } else {
                    event.getToolTip().add("§c[Придёт в негодность через " + (expireDate.getExpire()) + getEnder(expireDate.getExpire()) + "]");
                    if (expireDate.getNutritionValue() != -666) {
                        event.getToolTip().add("§2[Восстанавливает " + expireDate.getNutritionValue() + " ед. еды]");
                    }
                }
            }
        }



        CostHandler costHandler = new CostHandler(event.getItemStack());
        String cost = costHandler.getCostString();
        if (!cost.isEmpty() && event.getEntityPlayer().isCreative()) {
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            event.getToolTip().add("§eЦена: " + cost);
        }

        TrapHandler trapHandler = new TrapHandler(event.getItemStack());
        String trapdamage = trapHandler.getTrapString();
        if (!trapdamage.isEmpty() && event.getEntityPlayer().isCreative()) {
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            event.getToolTip().add("§4[Ловушка. Урон взрыва: " + trapdamage + "]");
        }
        if (!trapdamage.isEmpty() && !event.getEntityPlayer().isCreative()) {
            if (!erm) {
                event.getToolTip().add("");
                erm = true;
            }
            event.getToolTip().add(TextFormatting.DARK_RED + "[Вы попали в ловушку, ГМы оповещены. Ждите]");
            ItemStack itemStack = event.getItemStack();
            if (this.lastItemStack != itemStack){
                Minecraft.getMinecraft().player.sendChatMessage("/dmsggm ГМ, я попался в ловушку в координатах X:" + Math.floor(event.getEntityPlayer().posX) + " Y:" + Math.floor(event.getEntityPlayer().posY) + " Z:" + Math.floor(event.getEntityPlayer().posZ) + " Измерение:" + Minecraft.getMinecraft().player.dimension);
                this.lastItemStack = itemStack;
            }
        }

        if (event.getItemStack().getTagCompound().hasKey("useable")) {
            NBTTagCompound useable = event.getItemStack().getTagCompound().getCompoundTag("useable");
            switch (useable.getString("type")) {
                case "grindstone":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Использований точильного камня: " + useable.getInteger("uses") + "]");
                    break;
                case "gunoil":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Оружийное масло]");
                    break;
                case "mana":
                    if (useable.hasKey("hidden") && useable.getInteger("hidden") == 1) {
                        event.getToolTip().add("");
                        event.getToolTip().add("§3[Восполняет неизвестное количество потенциала]");
                    } else {
                        event.getToolTip().add("");
                        event.getToolTip().add("§3[Восполняет §b" + useable.getInteger("regen") + " §3потенциала]");
                    }
                    break;
                case "energy":
                    if (useable.hasKey("hidden") && useable.getInteger("hidden") == 1) {
                        event.getToolTip().add("");
                        event.getToolTip().add("§6[Восполняет неизвестное количество энергии]");
                    } else {
                        event.getToolTip().add("");
                        event.getToolTip().add("§6[Восполняет §2" + useable.getInteger("regen") + " §6энергии]");
                    }
                    break;
                case "melee_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта ближнего]");
                    break;
                case "ranged_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта дальнего]");
                    break;
                case "leather_patch":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Кожаная заплата]");
                    break;
                case "steel_patch":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Стальная заплата]");
                    break;
                case "light_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта легкой брони и щитов]");
                    break;
                case "heavy_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта тяжелой брони и щитов]");
                    break;
                case "stimulator":
                    event.getToolTip().add("");
                    event.getToolTip().add("§3[Стимулятор]");
                    if (event.getEntityPlayer().isCreative()) {
                        event.getToolTip().add("§7["+(useable.hasKey("trait") ? "Трейт: " + useable.getString("trait") + " ": "") +(useable.hasKey("skills") ? "Скиллы: " + useable.getString("skills") + " Бафф: " + useable.getInteger("buff") + " " : "") + "Ходы: " + useable.getInteger("turns") +" Отходняк: " + useable.getInteger("withdrawal") +" Штраф:" + useable.getInteger("debuff") + "]");
                    }
                    break;
                case "ointment":
                    event.getToolTip().add("");
                    event.getToolTip().add("§2[Мазь]");
                    break;
                case "lottery":
                    if (event.getEntityPlayer().isCreative()) {
                        event.getToolTip().add("");
                        if (useable.getString("id").equals("-1")) {
                            event.getToolTip().add("§2[ГМ: чистый билет, можно свободно копировать!!!]");
                        } else {
                            event.getToolTip().add("§4§l[ГМ: использованный билет, НЕЛЬЗЯ копировать!!!]");
                        }
                    }
            }
        }

        if (event.getItemStack().getTagCompound().hasKey("medical")) {
            NBTTagCompound useable = event.getItemStack().getTagCompound();
            switch (useable.getString("medical")) {
                case "bandage":
                    event.getToolTip().add("");
                    event.getToolTip().add("§b[Медицинский бинт]");
                    break;
            }
        }

        if (event.getItemStack().getTagCompound().hasKey("canbeplaced")) {
            event.getToolTip().add("");
            event.getToolTip().add("§2[Блок можно ставить несмотря на лор!]");
        }

        if (event.getItemStack().getTagCompound().hasKey("radiomic")) {
            NBTTagCompound radiomic = event.getItemStack().getTagCompound().getCompoundTag("radiomic");
            event.getToolTip().add("");
            event.getToolTip().add("§7[Радиомикрофон привязан к ID §f" + radiomic.getString("id") + "§7]");
        }

        // GM LORE
        if (GmLoreHandler.hasGmLore(event.getItemStack())) {
            if (!event.getEntityPlayer().isCreative()) return;
            GmLoreHandler gmLoreHandler = new GmLoreHandler(event.getItemStack());

            NBTTagList gmLore = gmLoreHandler.getGmLore();
            event.getToolTip().add("§6[ГМ-лор]");
            //event.getToolTip().add(String.valueOf(gmLore));
            for (int i = 0; i < gmLore.tagCount(); i++) {
                event.getToolTip().add(gmLore.getStringTagAt(i));
            }
        }

        // SKILLSAVER
//        if (SkillSaverHandler.hasCompound(event.getItemStack())) {
//            event.getToolTip().add("");
//            SkillSaverHandler ssv = new SkillSaverHandler(event.getItemStack());
//            NBTTagCompound result = ssv.getFullSkillSaverFor(event.getEntityPlayer().getName());
//
//            for (String string : result.getKeySet()) {
//                event.getToolTip().add(TextFormatting.GRAY + "[" + string + ": " + result.getString(string) + "]");
//            }
//        }

        // GM TAG
        if (GmTagHandler.hasGmTag(event.getItemStack())) {
            GmTagHandler gmTagHandler = new GmTagHandler(event.getItemStack());
            String tag = gmTagHandler.getGmTag();
            event.getToolTip().add("");

            if (gmTagHandler.isHidden() && !event.getEntityPlayer().isCreative()) return;
            String hidden = "";
            if (gmTagHandler.isHidden()) hidden = TextFormatting.GRAY + " (c)";
            event.getToolTip().add(TextFormatting.AQUA + "[" + tag + "]" + hidden);

        }


        // AMMO LORE
        if (AttachmentsHandler.isProjectile(event.getItemStack())) {
            AttachmentsHandler attachmentsHandler = new AttachmentsHandler(event.getItemStack());
            event.getToolTip().add("");

            String caliber = attachmentsHandler.getProjectileOrCreate().getString("caliber");
            if (attachmentsHandler.getProjectileOrCreate().hasKey("energy")) {
                int energy = attachmentsHandler.getProjectileOrCreate().getInteger("energy");
                int maxenergy = attachmentsHandler.getProjectileOrCreate().getInteger("maxenergy");
                int percentage = (int) (((double) energy /maxenergy) * 100);
                String color = "";
                if (percentage > 75) color = "§2";
                else if (percentage > 50) color = "§6";
                else if (percentage > 25) color = "§e";
                else if (percentage > 0) color = "§c";
                else color = "§8";
                event.getToolTip().add(TextFormatting.DARK_RED + "Энергоноситель " + caliber + " " + energy + "/" + maxenergy + " " + color + "[" + percentage + "%]");
            } else {
                int damage = attachmentsHandler.getProjectileOrCreate().getInteger("damage");
                String damageStr = String.valueOf(damage);
                if (damage >= 0) damageStr = "+" + damageStr;

                String mod = attachmentsHandler.getProjectileOrCreate().getString("mod");
                if (!mod.isEmpty()) mod = mod.toLowerCase();

                event.getToolTip().add(TextFormatting.DARK_RED + "Патрон " + caliber + " " + mod + " [" + damageStr + "]");
            }

            if (attachmentsHandler.getProjectileOrCreate().hasKey("modsplus")) {
                String modsplus = attachmentsHandler.getProjectileOrCreate().getString("modsplus");
                if (!modsplus.isEmpty()) {
                    event.getToolTip().add("§6Дополнительные эффекты:");
                    event.getToolTip().add("§6[" + modsplus + "]");
                }
            }
            if(attachmentsHandler.getProjectileOrCreate().hasKey("diff")) event.getToolTip().add("§eСложность: " + getResultAsString(attachmentsHandler.getProjectileOrCreate().getInteger("diff")));
            if(attachmentsHandler.getProjectileOrCreate().hasKey("cost")) {
                if(attachmentsHandler.getProjectileOrCreate().getInteger("cost") > 0) event.getToolTip().add("§bСтоимость в потенциале: " + attachmentsHandler.getProjectileOrCreate().getInteger("cost"));
            }

            if (attachmentsHandler.getProjectileOrCreate().hasKey("homemade")) {
                int homemade = attachmentsHandler.getProjectileOrCreate().getInteger("homemade");
                if (homemade > 0) {
                    event.getToolTip().add(TextFormatting.DARK_RED + "Шанс заклинить: " + homemade + "%");
                }
            }


        }

        if (WeaponTagsHandler.isMagazine(event.getItemStack())) {
            AttachmentsHandler attachmentsHandler = new AttachmentsHandler(event.getItemStack());
            event.getToolTip().add("");
            event.getToolTip().add(TextFormatting.RED + "[" + attachmentsHandler.getLoadedStuff().tagCount() + "/" +
                    attachmentsHandler.getMagazineSize() +
                    "] " + TextFormatting.GRAY +
                    attachmentsHandler.getMagazineCaliber()
            );

            if (attachmentsHandler.getLoadedStuff().tagCount() > 0) {
                NBTTagCompound upperAmmo = (NBTTagCompound) attachmentsHandler.getLoadedStuff().get(attachmentsHandler.getLoadedStuff().tagCount() - 1);
                event.getToolTip().add(TextFormatting.DARK_RED + "Верхний патрон: " + ammoToString(upperAmmo.getTag("projectile")));
            }
        }

        if (AttachmentsHandler.isModule(event.getItemStack())) {
            AttachmentsHandler attachmentsHandler = new AttachmentsHandler(event.getItemStack());
            event.getToolTip().add("");
            int mod = attachmentsHandler.getMod();
            if (mod > 0) {
                event.getToolTip().add("Процентный бонус модуля: +" + mod);
            } else if (mod < 0) {
                event.getToolTip().add("Процентный штраф модуля: " + mod);
            }
        }


//         event.getToolTip().add("");

    }

    public static String ammoToString(NBTBase ammoNBT) {
        String result = "";
        try {
            NBTTagCompound ammo = (NBTTagCompound) ammoNBT;
            result = ammo.getString("caliber");
            if (ammo.hasKey("energy") && ammo.hasKey("maxenergy")) {
                int energy = ammo.getInteger("energy");
                int maxenergy = ammo.getInteger("maxenergy");
                int percentage = (int) (((double) energy /maxenergy) * 100);
                String color = "";
                if (percentage > 75) color = "§2";
                else if (percentage > 50) color = "§6";
                else if (percentage > 25) color = "§e";
                else if (percentage > 0) color = "§c";
                else color = "§8";
                result += " " + color + "[" + percentage + "%]";
                return result;
            }
            int damage = ammo.getInteger("damage");

            String damageStr = String.valueOf(damage);
            if (damage >= 0) damageStr = "+" + damage;
            result += " " + damageStr;

            String mod = ammo.getString("mod");
            if (!mod.isEmpty()) mod = mod.toLowerCase();
            result += " " + mod;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


        return result;
    }

    public static void setCurrentDay(int day) {
        currentDay = day;
    }
    public String getEnder(int damage) {
        int preLastDigit = damage % 100 / 10;
        if (preLastDigit == 1) {
            return " дней";
        }

        switch (damage % 10) {
            case 1:
                return " день";
            case 2:
            case 3:
            case 4:
                return " дня";
            default:
                return " дней";
        }

    }

}
