package ru.konungstvo.kmrp_lore;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.server.FMLServerHandler;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.entity.data.IData;
import org.apache.logging.log4j.Logger;
import ru.konungstvo.kmrp_lore.command.*;
import ru.konungstvo.kmrp_lore.helpers.ExpireDate;
import ru.konungstvo.kmrp_lore.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.network.PacketMessage;
import ru.konungstvo.kmrp_lore.network.PacketMessageHandler;
import ru.konungstvo.kmrp_lore.network.currentDayMessage;

import static net.minecraftforge.common.MinecraftForge.EVENT_BUS;
import static ru.konungstvo.kmrp_lore.helpers.ExpireDate.EXPIRE;
import static ru.konungstvo.kmrp_lore.helpers.ExpireDate.hasExpiration;

public class CommonProxy {
    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @Mod.EventHandler
    public void startServer(FMLServerStartingEvent event) {
        System.out.println("KMSystem [FORGE] is starting!");
        EVENT_BUS.register(this);
        KMPacketHandler.INSTANCE.registerMessage(PacketMessageHandler.class, PacketMessage.class, 1, Side.SERVER);
        KMPacketHandler.INSTANCE.registerMessage(currentDayMessage.currentDayMessageHandler.class, currentDayMessage.class, 2, Side.CLIENT);

        PermissionAPI.registerNode("km.gm", DefaultPermissionLevel.OP, "Game Master permission");

        event.registerServerCommand(new RenameCommand());
        event.registerServerCommand(new AddloreCommand());
        event.registerServerCommand(new PurgeloreCommand());
        event.registerServerCommand(new SetloreCommand());
        event.registerServerCommand(new DelloreCommand());
        event.registerServerCommand(new NewloreCommand());
        event.registerServerCommand(new InloreCommand());

        event.registerServerCommand(new GmLoreCommand());
        event.registerServerCommand(new WeaponTagsCommand());
        event.registerServerCommand(new EquipmentBuffExecutor());
        event.registerServerCommand(new ExpireCommand());
        event.registerServerCommand(new SetWeightCommand());
        event.registerServerCommand(new SetCostCommand());
        event.registerServerCommand(new SetTrapCommand());
        event.registerServerCommand(new SetMeleeMagCommand());
        event.registerServerCommand(new WeaponDurabilityCommand());
        event.registerServerCommand(new AttachmentsCommand());
        event.registerServerCommand(new DebugCommand());
        event.registerServerCommand(new ModuleCommand());
        event.registerServerCommand(new BoltCommand());
        event.registerServerCommand(new ConfirmLore());

        event.registerServerCommand(new CopyLore());
        event.registerServerCommand(new PasteLore());
        event.registerServerCommand(new NRPCommand());

        event.registerServerCommand(new ArmorTagCommand());


        //NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());

    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {
//        event.player.openGui(this, 0, event.player.world,
//                (int) event.player.posX, (int) event.player.posY, (int) event.player.posZ);
        System.out.println("TEST SENDING PACKAGE");
        KMPacketHandler.INSTANCE.sendTo(new currentDayMessage(getCurrentDay()), (EntityPlayerMP) event.player);
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void eveeeent(PlayerContainerEvent.Open event) {
        if (event.getEntityPlayer().isCreative()) return;
        for (Slot slot : event.getContainer().inventorySlots) {
            if (!slot.getHasStack()) continue;
            if (hasExpiration(slot.getStack())) {
                ExpireDate expireDate = new ExpireDate(slot.getStack());
                if (expireDate.getExpireDate() == -666 && !expireDate.neverExpired()) {
                    expireDate.startExpiration(getCurrentDay());
                    System.out.println("Expiration started test " + slot.getStack().getDisplayName() + " " + expireDate.getExpireDate());
                }
            }
        }
    }

    public static int getCurrentDay() {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (data.has("currentDay")) {
                System.out.println("TEST SENDING PACKAGE" + data.get("currentDay"));
                return Integer.parseInt((String) data.get("currentDay"));
            } else {
                return -666;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return -666;
        }
    }

}
